﻿using Data.Interfaces;
using System.Linq;

namespace DataHelper
{
    public static class DataHelper
    {
        public static string GetFullString(this IDataContainer dataContainer)
        {
            return $"Your random advice is\n{ dataContainer.GetRandomAdvice() }";
        }

        public static string[] GetAllFullStrings(this IDataContainer dataContainer)
        {
            return (from advice in dataContainer.GetAllAdvices()
                    select $"Your advice is: {advice}").ToArray();
        }
    }
}
