﻿namespace Manager.Interfaces
{
    public interface IAppManager
    {
        void ShowAddingMenu();
        bool ShowAllAdvices();
        void ShowDeletingMenuByIndex();
        void ShowMainMenu();
        void ShowRandomAdvice();
    }
}