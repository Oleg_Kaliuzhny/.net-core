﻿namespace Data.Interfaces
{
    public interface IDataContainer
    {
        bool AddAdvice(string newAdvice);
        bool DeleteAdvice(string advice);
        bool DeleteAdvice(int advice);
        string GetRandomAdvice();
        bool Load();
        void Save();
        string[] GetAllAdvices();
    }
}