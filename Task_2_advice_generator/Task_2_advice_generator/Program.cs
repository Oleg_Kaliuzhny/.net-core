﻿using Data;
using Manager;
using Manager.Interfaces;

namespace Task_2_advice_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            IAppManager appManager = new AppManager(new DataContainer());
            //Main menu has a case for exiting
            // so stay always in this loop
            while (true)
            {
                appManager.ShowMainMenu();
            }
        }
    }
}
