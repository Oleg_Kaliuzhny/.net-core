﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Data.Interfaces;
using System.Linq;

namespace Data
{
    public class DataContainer : IDataContainer
    {
        public DataContainer()
        {
            _advices = new List<string>();
            _xmlSerializer = new XmlSerializer(typeof(List<string>));
            _filename = "advices.xml";
        }

        private IList<string> _advices;
        private XmlSerializer _xmlSerializer;
        private string _filename;

        /// <summary>
        /// Add new advice
        /// </summary>
        /// <param name="newAdvice">Your new advice</param>
        /// <returns>False if advice already in the list</returns>
        public bool AddAdvice(string newAdvice)
        {
            // check if new advice already in the advice list then don't add it
            foreach (var advice in _advices)
            {
                if (advice == newAdvice)
                    return false;
            }
            _advices.Add(newAdvice);
            return true;
        }

        /// <summary>
        /// Returns random advice
        /// </summary>
        /// <returns></returns>
        public string GetRandomAdvice()
        {
            if (_advices.Count == 0)
                return "";
            Random random = new Random();
            int index = random.Next() % _advices.Count;
            return _advices[index];
        }

        /// <summary>
        /// Returns an array with all advices
        /// </summary>
        /// <returns></returns>
        public string[] GetAllAdvices()
        {
            return _advices.ToArray();
        }

        /// <summary>
        /// Delete existing advice
        /// </summary>
        /// <param name="advice">Advice to delete</param>
        /// <returns>False if advice isn't in the list</returns>
        public bool DeleteAdvice(string advice)
        {
            return _advices.Remove(advice);
        }

        /// <summary>
        /// Delete existing advice
        /// </summary>
        /// <param name="advice">Advice to delete</param>
        /// <returns>False if advice isn't in the list</returns>
        public bool DeleteAdvice(int adviceIndex)
        {
            if (adviceIndex > _advices.Count)
                return false;
            _advices.RemoveAt(adviceIndex);
            return true;
        }

        /// <summary>
        /// Save all advices to the xml file. Filename is advices.xml
        /// </summary>
        public void Save()
        {
            if (File.Exists("advices.xml"))
                File.Delete("advices.xml");
            using (FileStream fs = new FileStream(_filename, FileMode.OpenOrCreate))
            {
                _xmlSerializer.Serialize(fs, _advices);
            }
        }

        /// <summary>
        /// Load all advices from the xml file.Filename is advices.xml
        /// </summary>
        public bool Load()
        {
            if (!File.Exists("advices.xml"))
                return false;
            using (FileStream fs = new FileStream(_filename, FileMode.Open))
            {
                List<string> advices = (List<string>)_xmlSerializer.Deserialize(fs);
                _advices = advices;
            }
            return true;
        }
    }
}
