﻿using Data.Interfaces;
using System;
using Manager.Interfaces;
using DataHelper;

namespace Manager
{
    public class AppManager : IAppManager
    {
        public AppManager(IDataContainer dataContainer)
        {
            _dataContainer = dataContainer;
        }

        private IDataContainer _dataContainer;

        public void ShowMainMenu()
        {
            Console.WriteLine("What do you want?");
            Console.WriteLine("1 - Get random advice");
            Console.WriteLine("2 - Add your advice");
            Console.WriteLine("3 - Delete existing advice");
            Console.WriteLine("4 - Load from file");
            Console.WriteLine("5 - Save to file");
            Console.WriteLine("6 - Show all saved advices");
            Console.WriteLine("Another - Exit");
            Console.Write("I want to: ");
            string answer = Console.ReadLine();
            Console.Clear();

            switch (answer)
            {
                case "1": // get advice
                    ShowRandomAdvice();
                    break;
                case "2": //add advice
                    ShowAddingMenu();
                    break;
                case "3": // delete
                    ShowDeletingMenuByIndex();
                    break;
                case "4": // load
                    if (!_dataContainer.Load())
                        Console.WriteLine("File doesn't exist!");
                    else
                        Console.WriteLine("Loaded!");
                    break;
                case "5": // save
                    _dataContainer.Save();
                    Console.WriteLine("Saved!");
                    break;
                case "6": // show all advices
                    ShowAllAdvices();
                    break;
                default: // exit
                    Environment.Exit(0);
                    break;
            }
        }

        public void ShowAddingMenu()
        {
            Console.WriteLine("What an advice do you want to add?");
            string answer = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(answer))
            {
                Console.WriteLine("It is an empty advice! Skipped.");
            }
            if (!_dataContainer.AddAdvice(answer))
                Console.WriteLine("Your advice already exists!");
        }

        public void ShowDeletingMenuByIndex()
        {
            if (!ShowAllAdvices())
            {
                Console.WriteLine("Advice list is empty!");
                return;
            }
            Console.WriteLine("Which advice do you want to delete?");
            Console.Write("Index: ");
            string answer = Console.ReadLine();
            int result = 0;
            if (int.TryParse(answer, out result))
                if (!_dataContainer.DeleteAdvice(result)) // delete advice by index
                {
                    Console.WriteLine("Your advice doesn't exist!");
                    return;
                }
                else
                {
                    Console.WriteLine("Deleted successfully!");
                    return;
                }
            Console.WriteLine("Your answer is not an index!");
        }

        public void ShowRandomAdvice()
        {
            string advice = _dataContainer.GetFullString();
            if (advice == "") // check if advice available
            {
                Console.WriteLine("There is no advice in the list!");
                return;
            }
            //Console.WriteLine("Advice for your:");
            Console.WriteLine(advice);
        }

        public bool ShowAllAdvices() // if no advice in the list then return false
        {
            string[] array = _dataContainer.GetAllFullStrings();
            if (array.Length == 0)
                return false;
            for (int index = 0; index < array.Length; index++)
            {
                Console.WriteLine("{0}: {1}", index, array[index]);
            }
            return true;
        }
    }
}
