﻿using ToDoListEntity.DAL.DTO.Interfaces;

namespace ToDoListEntity.DAL.DTO
{
    public class Status : IStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
