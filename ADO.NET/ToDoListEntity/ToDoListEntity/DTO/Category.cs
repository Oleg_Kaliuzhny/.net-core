﻿using ToDoListEntity.DAL.DTO.Interfaces;

namespace ToDoListEntity.DAL.DTO
{
    public class Category : ICategory
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
    }
}
