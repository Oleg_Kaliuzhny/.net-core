﻿using ToDoListEntity.DAL.DTO.Interfaces;

namespace ToDoListEntity.DAL.DTO
{
    public class Comment : IComment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CommentText { get; set; }
        public int? ReplyTo { get; set; }
        public int TaskId { get; set; }
    }
}
