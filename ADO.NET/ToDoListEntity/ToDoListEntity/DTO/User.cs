﻿using ToDoListEntity.DAL.DTO.Interfaces;

namespace ToDoListEntity.DAL.DTO
{
    public class User : IUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
