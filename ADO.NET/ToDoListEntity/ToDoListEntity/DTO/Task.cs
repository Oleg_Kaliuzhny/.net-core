﻿using System;
using ToDoListEntity.DAL.DTO.Interfaces;

namespace ToDoListEntity.DAL.DTO
{
    public class Task : ITask
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public int? StatusId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? BeginningDate { get; set; }
        public DateTime? FinishingDate { get; set; }
        public short? RemindHour { get; set; }
        public int? CategoryId { get; set; }
        public int? Attributes { get; set; }
    }
}
