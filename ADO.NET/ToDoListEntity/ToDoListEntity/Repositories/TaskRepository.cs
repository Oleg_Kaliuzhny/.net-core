﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListEntity.DAL.DTO;
using ToDoListEntity.DAL.DTO.Interfaces;
using ToDoListEntity.DAL.Repositories.Interfaces;
using ToDoListEntity.Model;

namespace ToDoListEntity.DAL.Repositories
{
    public class TaskRepository : IRepository<Model.Task, ITask>
    {
        public int Create(ITask model)
        {
            var task = DtoToEntity(model);

            using (var context = new Model.ToDoListEntities())
            {
                context.Tasks.Add(task);

                context.SaveChanges();
            }

            return task.Id;
        }

        public bool Create(IEnumerable<ITask> model)
        {
            var taskEntities = model
                .Select(DtoToEntity);

            bool result = false;

            using (var context = new Model.ToDoListEntities())
            {
                context.Tasks.AddRange(taskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var task = context.Tasks
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (task != null)
                {
                    context.Tasks.Remove(task);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<ITask> Get(Func<Model.Task, bool> predicate)
        {
            var result = new List<ITask>();

            using (var context = new Model.ToDoListEntities())
            {
                result = context.Tasks
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }

        public IEnumerable<ITask> Get(int skip, int take)
        {
            var result = new List<ITask>();

            using (var context = new Model.ToDoListEntities())
            {
                var modelResult = context.Tasks
                    .OrderBy(x => x.Id)
                    .Skip(skip)
                    .Take(take)
                    .Select(o => o)
                    .ToList();

                if (modelResult != null)
                {
                    result.Capacity = modelResult.Capacity;

                    foreach (var item in modelResult)
                    {
                        result.Add(EntityToDto(item));
                    }
                }
            }

            return result;
        }

        public void Update(ITask model)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var modelToUpdate = context.Tasks
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Attributes = model.Attributes;
                    modelToUpdate.BeginningDate = model.BeginningDate;
                    modelToUpdate.CategoryId = model.CategoryId;
                    modelToUpdate.CreationDate = model.CreationDate;
                    modelToUpdate.Description = model.Description;
                    modelToUpdate.FinishingDate = model.FinishingDate;
                    modelToUpdate.Priority = model.Priority;
                    modelToUpdate.RemindHour = model.RemindHour;
                    modelToUpdate.StatusId = model.StatusId;
                    modelToUpdate.Title = model.Title;
                    modelToUpdate.UserId = model.UserId;
                    context.SaveChanges();
                }
            }
        }

        private Model.Task DtoToEntity(ITask task)
        {
            return new Model.Task
            {
                Id = task.Id,
                Attributes = task.Attributes,
                BeginningDate = task.BeginningDate,
                CategoryId = task.CategoryId,
                CreationDate = task.CreationDate,
                Description = task.Description,
                FinishingDate = task.FinishingDate,
                Priority = task.Priority,
                RemindHour = task.RemindHour,
                StatusId = task.StatusId,
                Title = task.Title,
                UserId = task.UserId
            };
        }

        private ITask EntityToDto(Model.Task task)
        {
            return new DAL.DTO.Task
            {
                Id = task.Id,
                Attributes = task.Attributes,
                BeginningDate = task.BeginningDate,
                CategoryId = task.CategoryId,
                CreationDate = task.CreationDate,
                Description = task.Description,
                FinishingDate = task.FinishingDate,
                Priority = task.Priority,
                RemindHour = task.RemindHour,
                StatusId = task.StatusId,
                Title = task.Title,
                UserId = task.UserId
            };
        }
    }
}
