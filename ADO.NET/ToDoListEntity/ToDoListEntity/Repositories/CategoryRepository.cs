﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListEntity.DAL.DTO.Interfaces;
using ToDoListEntity.DAL.Repositories.Interfaces;

namespace ToDoListEntity.DAL.Repositories
{
    public class CategoryRepository : IRepository<Model.Category, ICategory>
    {
        public int Create(ICategory model)
        {
            var category = DtoToEntity(model);

            using (var context = new Model.ToDoListEntities())
            {
                context.Categories.Add(category);

                context.SaveChanges();
            }

            return category.Id;
        }

        public bool Create(IEnumerable<ICategory> model)
        {
            var categoryEntities = model
                .Select(DtoToEntity);

            bool result = false;

            using (var context = new Model.ToDoListEntities())
            {
                context.Categories.AddRange(categoryEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var category = context.Categories
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (category != null)
                {
                    context.Categories.Remove(category);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<ICategory> Get(Func<Model.Category, bool> predicate)
        {
            var result = new List<ICategory>();

            using (var context = new Model.ToDoListEntities())
            {
                result = context.Categories
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }

        public IEnumerable<ICategory> Get(int skip, int take)
        {
            List<ICategory> result = new List<ICategory>();

            using (var context = new Model.ToDoListEntities())
            {
                var modelResult = context.Categories
                    .OrderBy(x => x.Id)
                    .Skip(skip)
                    .Take(take)
                    .Select(o => o)
                    .ToList();

                if (modelResult != null)
                {
                    result.Capacity = modelResult.Capacity;

                    foreach (var item in modelResult)
                    {
                        result.Add(EntityToDto(item));
                    }
                }
            }

            return result;
        }

        public void Update(ICategory model)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var modelToUpdate = context.Categories
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.CategoryName = model.CategoryName;
                    modelToUpdate.CategoryDescription = model.CategoryDescription;
                    context.SaveChanges();
                }
            }
        }

        private Model.Category DtoToEntity(ICategory category)
        {
            return new Model.Category
            {
                Id = category.Id,
                CategoryDescription = category.CategoryDescription,
                CategoryName = category.CategoryName
            };
        }

        private ICategory EntityToDto(Model.Category category)
        {
            return new DAL.DTO.Category
            {
                Id = category.Id,
                CategoryDescription = category.CategoryDescription,
                CategoryName = category.CategoryName
            };
        }
    }
}
