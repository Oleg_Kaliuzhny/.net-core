﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListEntity.DAL.DTO.Interfaces;
using ToDoListEntity.DAL.Repositories.Interfaces;
using ToDoListEntity.Model;

namespace ToDoListEntity.DAL.Repositories
{
    public class StatusRepository : IRepository<Model.Status, IStatus>
    {
        public int Create(IStatus model)
        {
            var status = DtoToEntity(model);

            using (var context = new Model.ToDoListEntities())
            {
                context.Statuses.Add(status);

                context.SaveChanges();
            }

            return status.Id;
        }

        public bool Create(IEnumerable<IStatus> model)
        {
            var statusEntities = model
                .Select(DtoToEntity);

            bool result = false;

            using (var context = new Model.ToDoListEntities())
            {
                context.Statuses.AddRange(statusEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var status = context.Statuses
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (status != null)
                {
                    context.Statuses.Remove(status);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<IStatus> Get(Func<Status, bool> predicate)
        {
            var result = new List<IStatus>();

            using (var context = new Model.ToDoListEntities())
            {
                result = context.Statuses
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }

        public IEnumerable<IStatus> Get(int skip, int take)
        {
            var result = new List<IStatus>();

            using (var context = new Model.ToDoListEntities())
            {
                var modelResult = context.Statuses
                    .OrderBy(x => x.Id)
                    .Skip(skip)
                    .Take(take)
                    .Select(o => o)
                    .ToList();

                if (modelResult != null)
                {
                    result.Capacity = modelResult.Capacity;

                    foreach (var item in modelResult)
                    {
                        result.Add(EntityToDto(item));
                    }
                }
            }

            return result;
        }

        public void Update(IStatus model)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var modelToUpdate = context.Statuses
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.StatusName = model.StatusName;
                    context.SaveChanges();
                }
            }
        }

        private Model.Status DtoToEntity(IStatus status)
        {
            return new Model.Status
            {
                Id = status.Id,
                StatusName = status.StatusName
            };
        }

        private IStatus EntityToDto(Model.Status status)
        {
            return new DAL.DTO.Status
            {
                Id = status.Id,
                StatusName = status.StatusName
            };
        }
    }
}
