﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListEntity.DAL.DTO;
using ToDoListEntity.DAL.DTO.Interfaces;
using ToDoListEntity.DAL.Repositories.Interfaces;
using ToDoListEntity.Model;

namespace ToDoListEntity.Repositories
{
    public class UserRepository : IRepository<Model.User, IUser>
    {
        public int Create(IUser model)
        {
            var user = DtoToEntity(model);

            using (var context = new Model.ToDoListEntities())
            {
                context.Users.Add(user);

                context.SaveChanges();
            }

            return user.Id;
        }

        public bool Create(IEnumerable<IUser> model)
        {
            var userEntities = model
                .Select(DtoToEntity);

            bool result = false;

            using (var context = new Model.ToDoListEntities())
            {
                context.Users.AddRange(userEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var user = context.Users
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (user != null)
                {
                    context.Users.Remove(user);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<IUser> Get(Func<Model.User, bool> predicate)
        {
            var result = new List<IUser>();

            using (var context = new Model.ToDoListEntities())
            {
                result = context.Users
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }

        public IEnumerable<IUser> Get(int skip, int take)
        {
            var result = new List<IUser>();

            using (var context = new Model.ToDoListEntities())
            {
                var modelResult = context.Users
                    .OrderBy(x => x.Id)
                    .Skip(skip)
                    .Take(take)
                    .Select(o => o)
                    .ToList();

                if (modelResult != null)
                {
                    result.Capacity = modelResult.Capacity;

                    foreach (var item in modelResult)
                    {
                        result.Add(EntityToDto(item));
                    }
                }
            }

            return result;
        }

        public void Update(IUser model)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var modelToUpdate = context.Users
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Email = model.Email;
                    modelToUpdate.Password = model.Password;
                    context.SaveChanges();
                }
            }
        }

        private Model.User DtoToEntity(IUser user)
        {
            return new Model.User
            {
                Id = user.Id,
                Email = user.Email,
                Password = user.Password
            };
        }

        private IUser EntityToDto(Model.User user)
        {
            return new DAL.DTO.User
            {
                Id = user.Id,
                Email = user.Email,
                Password = user.Password
            };
        }
    }
}
