﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListEntity.DAL.Repositories.Interfaces;
using ToDoListEntity.DAL.DTO.Interfaces;
using ToDoListEntity.Model;

namespace ToDoListEntity.DAL.Repositories
{
    public class CommentRepository : IRepository<Model.Comment, IComment>
    {
        public int Create(IComment model)
        {
            var comment = DtoToEntity(model);

            using (var context = new Model.ToDoListEntities())
            {
                context.Comments.Add(comment);

                context.SaveChanges();
            }

            return comment.Id;
        }

        public bool Create(IEnumerable<IComment> model)
        {
            var commentEntities = model
                .Select(DtoToEntity);

            bool result = false;

            using (var context = new Model.ToDoListEntities())
            {
                context.Comments.AddRange(commentEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var comment = context.Comments
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (comment != null)
                {
                    context.Comments.Remove(comment);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<IComment> Get(Func<Comment, bool> predicate)
        {
            var result = new List<IComment>();

            using (var context = new Model.ToDoListEntities())
            {
                result = context.Comments
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }

        public IEnumerable<IComment> Get(int skip, int take)
        {
            var result = new List<IComment>();

            using (var context = new Model.ToDoListEntities())
            {
                var modelResult = context.Comments
                    .OrderBy(x => x.Id)
                    .Skip(skip)
                    .Take(take)
                    .Select(o => o).ToList();

                if (modelResult != null)
                {
                    result.Capacity = modelResult.Capacity;

                    foreach (var item in modelResult)
                    {
                        result.Add(EntityToDto(item));
                    }
                }
            }

            return result;
        }

        public void Update(IComment model)
        {
            using (var context = new Model.ToDoListEntities())
            {
                var modelToUpdate = context.Comments
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.CommentText = model.CommentText;
                    modelToUpdate.ReplyTo = model.ReplyTo;
                    modelToUpdate.TaskId = model.TaskId;
                    modelToUpdate.UserId = model.UserId;
                    context.SaveChanges();
                }
            }
        }

        private Model.Comment DtoToEntity(IComment comment)
        {
            return new Model.Comment
            {
                Id = comment.Id,
                CommentText = comment.CommentText,
                ReplyTo = comment.ReplyTo,
                TaskId = comment.TaskId,
                UserId = comment.UserId
            };
        }

        private IComment EntityToDto(Model.Comment comment)
        {
            return new DAL.DTO.Comment
            {
                Id = comment.Id,
                CommentText = comment.CommentText,
                ReplyTo = comment.ReplyTo,
                TaskId = comment.TaskId,
                UserId = comment.UserId
            };
        }
    }
}
