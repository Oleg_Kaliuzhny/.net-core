﻿using System;
using EntityCodeFirst.Repositories;
using EntityCodeFirst.Entities.Interfaces;
//using ToDoListEntity.DAL.Repositories.Interfaces;
//using ToDoListEntity.DAL.Repositories;

namespace ToDoListConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = new CategoryRepository();
            //var res = repo.Read(x => x.Id == 1);
            //var res = repo.Get(x => x.Id < 10);
            var res = repo.Read(0, 10);
            //repo.Delete(res[2].Id);

            foreach (var item in res)
            {
                Console.WriteLine($"{item.Id} : {item.CategoryName} : {item.CategoryDescription}");
            }

            Console.WriteLine("End");
            Console.ReadKey();
        }
    }
}
