﻿namespace EntityCodeFirst.Entities.Interfaces
{
    public interface IComment
    {
        int Id { get; set; }
        int? ReplyTo { get; set; }
        int TaskId { get; set; }
        string Text { get; set; }
        int UserID { get; set; }
    }
}