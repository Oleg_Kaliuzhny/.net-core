﻿namespace EntityCodeFirst.Entities.Interfaces
{
    public interface IStatus
    {
        int Id { get; set; }
        string StatusName { get; set; }
    }
}