﻿namespace EntityCodeFirst.Entities.Interfaces
{
    public interface ICategory
    {
        int Id { get; set; }
        string CategoryDescription { get; set; }
        string CategoryName { get; set; }
    }
}