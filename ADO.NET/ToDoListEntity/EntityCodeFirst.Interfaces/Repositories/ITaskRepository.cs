﻿using System;
using System.Collections.Generic;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories.Interfaces
{
    public interface ITaskRepository
    {
        int Create(int userId, string title, int priority, int? statusId, DateTime creationDate, DateTime beginningDate, DateTime finishingDate, short? remindHour, int? categoryId, int? attributes, string description);
        void Delete(int id);
        List<ITask> Read(Func<ITask, bool> predicate);
        List<ITask> Read(int skip, int count);
        void Update(int id, int userId, string title, int priority, int? statusId, DateTime creationDate, DateTime beginningDate, DateTime finishingDate, short? remindHour, int? categoryId, int? attributes, string description);
    }
}