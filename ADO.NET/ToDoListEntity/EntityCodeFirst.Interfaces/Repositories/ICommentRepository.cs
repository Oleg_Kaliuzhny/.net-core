﻿using System;
using System.Collections.Generic;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories.Interfaces
{
    public interface ICommentRepository
    {
        int Create(string text, int userID, int taskId, int? replyTo = null);
        void Delete(int id);
        List<IComment> Read(Func<IComment, bool> predicate);
        List<IComment> Read(int skip, int count);
        void Update(int id, string newText, int newUserID, int newTaskId, int? newReplyTo = null);
    }
}