﻿using System;
using System.Collections.Generic;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories.Interfaces
{
    public interface IUserRepository
    {
        int Create(string email, string password);
        void Delete(int id);
        List<IUser> Read(Func<IUser, bool> predicate);
        List<IUser> Read(int skip, int count);
        void Update(int id, string email, string password);
    }
}