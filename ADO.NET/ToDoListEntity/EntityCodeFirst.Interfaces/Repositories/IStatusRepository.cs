﻿using System;
using System.Collections.Generic;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories.Interfaces
{
    public interface IStatusRepository
    {
        int Create(string statusName);
        void Delete(int id);
        List<IStatus> Read(Func<IStatus, bool> predicate);
        List<IStatus> Read(int skip, int count);
        void Update(int id, string newStatusName);
    }
}