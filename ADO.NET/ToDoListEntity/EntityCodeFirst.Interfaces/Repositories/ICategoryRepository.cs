﻿using System;
using System.Collections.Generic;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        int Create(string categoryName, string categoryDescription = null);
        void Delete(int id);
        List<ICategory> Read(Func<ICategory, bool> predicate);
        List<ICategory> Read(int skip, int count);
        void Update(int id, string newCategoryName, string newCategoryDescription = null);
    }
}