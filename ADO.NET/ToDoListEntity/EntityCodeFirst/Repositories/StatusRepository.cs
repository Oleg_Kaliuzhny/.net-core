﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityCodeFirst.Entities;
using EntityCodeFirst.Repositories.Interfaces;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories
{
    public class StatusRepository : IStatusRepository
    {
        public int Create(string statusName)
        {
            if (string.IsNullOrEmpty(statusName))
            {
                throw new Exception("Status name cannot be empty.");
            }

            var newStatus = new Status()
            {
                StatusName = statusName
            };


            using (var context = new Context())
            {
                context.Statuses.Add(newStatus);
                context.SaveChanges();
            }

            return newStatus.Id;
        }

        public void Update(int id, string newStatusName)
        {
            if (string.IsNullOrEmpty(newStatusName))
            {
                throw new Exception("Status name cannot be empty.");
            }

            using (var context = new Context())
            {
                var rowForUpdate = context.Statuses.First(x => x.Id == id);

                rowForUpdate.StatusName = newStatusName;

                context.SaveChanges();
            }
        }

        public List<IStatus> Read(int skip, int count)
        {
            using (var context = new Context())
            {
                var selected = context.Statuses.OrderBy(x => x.Id)
                    .Skip(skip).Take(count)
                    .ToList<IStatus>();

                return selected;
            }
        }

        public List<IStatus> Read(Func<IStatus, bool> predicate)
        {
            using (var context = new Context())
            {
                var selected = context.Statuses.OrderBy(x => x.Id)
                    .Where(predicate)
                    .ToList<IStatus>();

                return selected;
            }
        }

        public void Delete(int id)
        {
            using (var context = new Context())
            {
                context.Statuses.Remove(context.Statuses.First(x => x.Id == id));
                context.SaveChanges();
            }
        }
    }
}
