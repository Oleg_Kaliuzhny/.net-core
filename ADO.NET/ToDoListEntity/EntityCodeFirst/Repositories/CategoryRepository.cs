﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityCodeFirst.Entities;
using EntityCodeFirst.Repositories.Interfaces;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        public int Create(string categoryName, string categoryDescription = null)
        {
            if (string.IsNullOrEmpty(categoryName))
            {
                throw new Exception("Category name cannot be empty.");
            }

            var newCategory = new Category()
            {
                CategoryName = categoryName,
                CategoryDescription = categoryDescription
            };


            using (var context = new Context())
            {
                context.Categories.Add(newCategory);
                context.SaveChanges();
            }

            return newCategory.Id;
        }

        public void Update(int id, string newCategoryName, string newCategoryDescription = null)
        {
            if (string.IsNullOrEmpty(newCategoryName))
            {
                throw new Exception("Category name cannot be empty.");
            }

            using (var context = new Context())
            {
                var rowForUpdate = context.Categories.First(x => x.Id == id);

                rowForUpdate.CategoryName = newCategoryName;
                rowForUpdate.CategoryDescription = newCategoryDescription;

                context.SaveChanges();
            }
        }

        public List<ICategory> Read(int skip, int count)
        {
            using (var context = new Context())
            {
                var selected = context.Categories.OrderBy(x => x.Id)
                    .Skip(skip).Take(count)
                    .ToList<ICategory>();

                return selected;
            }
        }

        public List<ICategory> Read(Func<ICategory, bool> predicate)
        {
            using (var context = new Context())
            {
                var selected = context.Categories.OrderBy(x => x.Id)
                    .Where(predicate)
                    .ToList<ICategory>();

                return selected;
            }
        }

        public void Delete(int id)
        {
            using (var context = new Context())
            {
                context.Categories.Remove(context.Categories.First(x => x.Id == id));
                context.SaveChanges();
            }
        }
    }
}
