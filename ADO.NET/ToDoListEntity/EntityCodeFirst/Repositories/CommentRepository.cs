﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityCodeFirst.Entities;
using EntityCodeFirst.Repositories.Interfaces;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        public int Create(string text, int userID, int taskId, int? replyTo = null)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new Exception("Comment text cannot be empty.");
            }

            var newComment = new Comment()
            {
                Text = text,
                ReplyTo = replyTo,
                UserID = userID,
                TaskId = taskId
            };


            using (var context = new Context())
            {
                context.Comments.Add(newComment);
                context.SaveChanges();
            }

            return newComment.Id;
        }

        public void Update(int id, string newText, int newUserID, int newTaskId, int? newReplyTo = null)
        {
            if (string.IsNullOrEmpty(newText))
            {
                throw new Exception("Comment text cannot be empty.");
            }

            using (var context = new Context())
            {
                var rowForUpdate = context.Comments.First(x => x.Id == id);

                rowForUpdate.Text = newText;
                rowForUpdate.UserID = newUserID;
                rowForUpdate.TaskId = newTaskId;
                rowForUpdate.ReplyTo = newReplyTo;

                context.SaveChanges();
            }
        }

        public List<IComment> Read(int skip, int count)
        {
            using (var context = new Context())
            {
                var selected = context.Comments.OrderBy(x => x.Id)
                    .Skip(skip).Take(count)
                    .ToList<IComment>();

                return selected;
            }
        }

        public List<IComment> Read(Func<IComment, bool> predicate)
        {
            using (var context = new Context())
            {
                var selected = context.Comments.OrderBy(x => x.Id)
                    .Where(predicate)
                    .ToList<IComment>();

                return selected;
            }
        }

        public void Delete(int id)
        {
            using (var context = new Context())
            {
                context.Comments.Remove(context.Comments.First(x => x.Id == id));
                context.SaveChanges();
            }
        }
    }
}
