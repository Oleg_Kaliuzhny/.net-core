﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityCodeFirst.Entities;
using EntityCodeFirst.Repositories.Interfaces;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories
{
    public class UserRepository : IUserRepository
    {
        public int Create(string email, string password)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("Email cannot be empty.");
            }
            else if (string.IsNullOrEmpty(password))
            {
                throw new Exception("Password cannot be empty.");
            }

            var newUser = new User()
            {
                Email = email,
                Password = password
            };


            using (var context = new Context())
            {
                context.Users.Add(newUser);
                context.SaveChanges();
            }

            return newUser.Id;
        }

        public void Update(int id, string email, string password)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("Email cannot be empty.");
            }
            else if (string.IsNullOrEmpty(password))
            {
                throw new Exception("Password cannot be empty.");
            }

            using (var context = new Context())
            {
                var rowForUpdate = context.Users.First(x => x.Id == id);

                rowForUpdate.Email = email;
                rowForUpdate.Password = password;

                context.SaveChanges();
            }
        }

        public List<IUser> Read(int skip, int count)
        {
            using (var context = new Context())
            {
                var selected = context.Users.OrderBy(x => x.Id)
                    .Skip(skip).Take(count)
                    .ToList<IUser>();

                return selected;
            }
        }

        public List<IUser> Read(Func<IUser, bool> predicate)
        {
            using (var context = new Context())
            {
                var selected = context.Users.OrderBy(x => x.Id)
                    .Where(predicate)
                    .ToList<IUser>();

                return selected;
            }
        }

        public void Delete(int id)
        {
            using (var context = new Context())
            {
                context.Users.Remove(context.Users.First(x => x.Id == id));
                context.SaveChanges();
            }
        }
    }
}
