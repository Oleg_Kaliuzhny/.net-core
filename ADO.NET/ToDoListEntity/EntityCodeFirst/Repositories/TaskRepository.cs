﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityCodeFirst.Entities;
using EntityCodeFirst.Repositories.Interfaces;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        public int Create(int userId, string title, int priority, int? statusId, DateTime creationDate, DateTime beginningDate,
            DateTime finishingDate, Int16? remindHour, int? categoryId, int? attributes, string description)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new Exception("Title cannot be empty.");
            }

            var newTask = new Task()
            {
                Attributes = attributes,
                BeginningDate = beginningDate,
                CategoryId = categoryId,
                CreationDate = creationDate,
                Description = description,
                FinishingDate = finishingDate,
                Priority = priority,
                RemindHour = remindHour,
                StatusId = statusId,
                Title = title,
                UserId = userId
            };


            using (var context = new Context())
            {
                context.Tasks.Add(newTask);
                context.SaveChanges();
            }

            return newTask.Id;
        }

        public void Update(int id, int userId, string title, int priority, int? statusId, DateTime creationDate, 
            DateTime beginningDate, DateTime finishingDate, Int16? remindHour, int? categoryId, int? attributes, string description)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new Exception("Title cannot be empty.");
            }

            using (var context = new Context())
            {
                var rowForUpdate = context.Tasks.First(x => x.Id == id);
                
                rowForUpdate.Attributes = attributes;
                rowForUpdate.BeginningDate = beginningDate;
                rowForUpdate.CategoryId = categoryId;
                rowForUpdate.CreationDate = creationDate;
                rowForUpdate.Description = description;
                rowForUpdate.FinishingDate = finishingDate;
                rowForUpdate.Priority = priority;
                rowForUpdate.RemindHour = remindHour;
                rowForUpdate.StatusId = statusId;
                rowForUpdate.Title = title;
                rowForUpdate.UserId = userId;

                context.SaveChanges();
            }
        }

        public List<ITask> Read(int skip, int count)
        {
            using (var context = new Context())
            {
                var selected = context.Tasks.OrderBy(x => x.Id)
                    .Skip(skip).Take(count)
                    .ToList<ITask>();

                return selected;
            }
        }

        public List<ITask> Read(Func<ITask, bool> predicate)
        {
            using (var context = new Context())
            {
                var selected = context.Tasks.OrderBy(x => x.Id)
                    .Where(predicate)
                    .ToList<ITask>();

                return selected;
            }
        }

        public void Delete(int id)
        {
            using (var context = new Context())
            {
                context.Tasks.Remove(context.Tasks.First(x => x.Id == id));
                context.SaveChanges();
            }
        }
    }
}
