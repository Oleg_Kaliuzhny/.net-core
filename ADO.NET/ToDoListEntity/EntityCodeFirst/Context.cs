﻿using System.Data.Entity;
using EntityCodeFirst.Entities;

namespace EntityCodeFirst
{
    public class Context : DbContext
    {
        public Context()
          : base("ToDoList")
        {

        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
