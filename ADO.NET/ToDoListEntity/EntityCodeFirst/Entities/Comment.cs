﻿using System.ComponentModel.DataAnnotations;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Entities
{
    public class Comment : BaseEntity, IComment
    {
        [Required]
        public int UserID { get; set; }

        [Required]
        [StringLength(500)]
        public string Text { get; set; }

        public int? ReplyTo { get; set; }

        [Required]
        public int TaskId { get; set; }
    }
}
