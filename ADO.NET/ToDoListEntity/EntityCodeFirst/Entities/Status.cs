﻿using System.ComponentModel.DataAnnotations;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Entities
{
    public class Status : BaseEntity, IStatus
    {
        [Required]
        [StringLength(50)]
        public string StatusName { get; set; }
    }
}
