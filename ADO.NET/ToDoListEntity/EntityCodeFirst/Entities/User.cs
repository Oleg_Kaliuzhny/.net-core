﻿using System.ComponentModel.DataAnnotations;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Entities
{
    public class User : BaseEntity, IUser
    {
        [StringLength(255)]
        public virtual string Email { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }
    }
}
