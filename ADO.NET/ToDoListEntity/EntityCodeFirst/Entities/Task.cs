﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Entities
{
    public class Task : BaseEntity, ITask
    {
        [Required]
        public int UserId { get; set; }

        [StringLength(255)]
        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        public int Priority { get; set; }

        public int? StatusId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime BeginningDate { get; set; }

        public DateTime FinishingDate { get; set; }

        public Int16? RemindHour { get; set; }

        public int? CategoryId { get; set; }

        public int? Attributes { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("StatusId")]
        public Status Status { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}
