﻿namespace EntityCodeFirst.Entities
{
    public interface IUser
    {
        int Id { get; set; }
        string Email { get; set; }
        string Password { get; set; }
    }
}