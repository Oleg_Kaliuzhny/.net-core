﻿using System.ComponentModel.DataAnnotations;
using EntityCodeFirst.Entities.Interfaces;

namespace EntityCodeFirst.Entities
{
    public class Category : BaseEntity, ICategory
    {
        [StringLength(50)]
        [Required]
        public virtual string CategoryName { get; set; }

        [StringLength(50)]
        public virtual string CategoryDescription { get; set; }

    }
}
