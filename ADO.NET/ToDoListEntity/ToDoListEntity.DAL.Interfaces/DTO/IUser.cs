﻿namespace ToDoListEntity.DAL.DTO.Interfaces
{
    public interface IUser
    {
        string Email { get; set; }
        int Id { get; set; }
        string Password { get; set; }
    }
}