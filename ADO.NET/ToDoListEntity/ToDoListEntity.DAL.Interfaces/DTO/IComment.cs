﻿namespace ToDoListEntity.DAL.DTO.Interfaces
{
    public interface IComment
    {
        string CommentText { get; set; }
        int Id { get; set; }
        int? ReplyTo { get; set; }
        int TaskId { get; set; }
        int UserId { get; set; }
    }
}