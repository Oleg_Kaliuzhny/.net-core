﻿using System;

namespace ToDoListEntity.DAL.DTO.Interfaces
{
    public interface ITask
    {
        int? Attributes { get; set; }
        DateTime? BeginningDate { get; set; }
        int? CategoryId { get; set; }
        DateTime? CreationDate { get; set; }
        string Description { get; set; }
        DateTime? FinishingDate { get; set; }
        int Id { get; set; }
        int Priority { get; set; }
        short? RemindHour { get; set; }
        int? StatusId { get; set; }
        string Title { get; set; }
        int UserId { get; set; }
    }
}