﻿namespace ToDoListEntity.DAL.DTO.Interfaces
{
    public interface ICategory
    {
        string CategoryDescription { get; set; }
        string CategoryName { get; set; }
        int Id { get; set; }
    }
}