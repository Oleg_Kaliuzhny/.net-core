﻿using System;
using System.Collections.Generic;

namespace ToDoListEntity.DAL.Repositories.Interfaces
{
    public interface IRepository<TEntity, TDto>
    {
        int Create(TDto model);
        bool Create(IEnumerable<TDto> model);
        void Update(TDto model);
        void Delete(int id);
        IEnumerable<TDto> Get(Func<TEntity, bool> predicate);
        IEnumerable<TDto> Get(int skip, int take);
    }
}
