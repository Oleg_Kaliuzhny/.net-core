﻿using System;
using Data.Database.Domains;
using System.Data;

namespace ADO_ToDoList
{
    class Program
    {
        static void Main(string[] args)
        {
            CategoryRepository categoryRepository = new CategoryRepository();
            StatusRepository statusRepository = new StatusRepository();
            CommentRepository commentRepository = new CommentRepository();
            TaskRepository taskRepository = new TaskRepository();
            UserRepository userRepository = new UserRepository();


            var catId = categoryRepository.AddNewCategory("Some tasks");
            //categoryRepository.GetAllCategories();
            //categoryRepository.GetCategory(1);
            categoryRepository.UpdateCategory(catId, "Hometasks");


            foreach (DataRow dataRow in categoryRepository.Data.Tables[0].Rows)
            {
                foreach (var item in dataRow.ItemArray)
                {
                    Console.Write("| {0} ", item);
                }
                Console.WriteLine();
            }

            //Console.WriteLine(categories.DeleteCategory(2));
            Console.WriteLine("End.");
            Console.ReadKey();
        }
    }
}
