﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace Data.Database.Domains
{
    public class UserRepository
    {
        //TODO
        //INSERT, UPDATE, DELETE range and add param of DataSet to change

        public UserRepository()
        {
            _connectionStringName = "ToDoList";
            _connectionDetails = ConfigurationManager.ConnectionStrings[_connectionStringName];
            _providerName = _connectionDetails.ProviderName;
            _connectionString = _connectionDetails.ConnectionString;
            _dbFactory = DbProviderFactories.GetFactory(_providerName);

            // Parameters
            _idParam = _dbFactory.CreateParameter();
            _idParam.DbType = DbType.Int32;
            _idParam.ParameterName = "@Id";
            _idParam.SourceColumn = "Id";

            _emailParam = _dbFactory.CreateParameter();
            _emailParam.DbType = DbType.String;
            _emailParam.ParameterName = "@Email";
            _emailParam.SourceColumn = "Email";

            _passwordParam = _dbFactory.CreateParameter();
            _passwordParam.DbType = DbType.String;
            _passwordParam.ParameterName = "@Password";
            _passwordParam.SourceColumn = "Password";

            //query templates
            _selectQuery = @"SELECT Id, Email, Password FROM Users";
            _insertQuery = @"INSERT INTO Users (Email, Password) VALUES (@Email, @Password)";
            _updateQuery = @"UPDATE Users SET Email = @Email, Password = @Password WHERE Id = @Id";
            _deleteQuery = @"DELETE FROM Users WHERE Id = @Id";

            _adapter = _dbFactory.CreateDataAdapter();
            _data = new DataSet();
            //init DataSet
            this.GetUser(-1);
        }

        private readonly string _connectionStringName;
        private ConnectionStringSettings _connectionDetails;
        private string _providerName;
        private string _connectionString;
        private string _selectQuery;
        private string _insertQuery;
        private string _updateQuery;
        private string _deleteQuery;
        private DbParameter _idParam;
        private DbParameter _emailParam;
        private DbParameter _passwordParam;
        private DbProviderFactory _dbFactory;
        private DbDataAdapter _adapter;

        private DataSet _data;

        public DataSet Data
        {
            get
            {
                return _data;
            }
            private set
            {
                _data = value;
                DataSetChanged?.Invoke();
            }
        }

        public void GetAllUsers()
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = _selectQuery;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                _adapter.Fill(_data);
            }
        }

        public void GetUser(int id, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE Id = {id}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public int AddNewUser(string email, string password)
        {
            var result = -1;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Configure Insert Command
                var insertCommand = _dbFactory.CreateCommand();
                insertCommand.Parameters.Add(_emailParam);
                insertCommand.Parameters.Add(_passwordParam);
                insertCommand.Connection = connection;
                insertCommand.CommandType = CommandType.Text;
                insertCommand.CommandText = _insertQuery;

                // Create and configure a DbDataAdapter
                _adapter.InsertCommand = insertCommand;
                var row = _data.Tables[0].Rows.Add(0, email, password);
                result = _adapter.Update(_data.Tables[0]);
            }
            return result;
        }

        public int UpdateUser(int id, string email, string password)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetUser(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0]["Email"] = email;
                    temporaryDataSet.Tables[0].Rows[0]["Password"] = password;
                }

                // Configure Update Command.
                var updateCommand = _dbFactory.CreateCommand();

                updateCommand.CommandText = _updateQuery;
                updateCommand.Parameters.Add(_idParam);
                updateCommand.Parameters.Add(_emailParam);
                updateCommand.Parameters.Add(_passwordParam);

                updateCommand.Connection = connection;
                updateCommand.CommandType = CommandType.Text;
                _adapter.UpdateCommand = updateCommand;

                // Create and configure a DbDataAdapter
                _adapter.UpdateCommand = updateCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public int DeleteUser(int id)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetUser(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0].Delete();
                }

                // Delete Command
                var deleteCommand = _dbFactory.CreateCommand();

                deleteCommand.CommandText = _deleteQuery;
                deleteCommand.Parameters.Add(_idParam);

                deleteCommand.Connection = connection;
                deleteCommand.CommandType = CommandType.Text;
                _adapter.DeleteCommand = deleteCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public event Action DataSetChanged;
    }
}
