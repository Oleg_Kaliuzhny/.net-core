﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace Data.Database.Domains
{
    public class CategoryRepository
    {
        //TODO
        //INSERT, UPDATE, DELETE range and add param of DataSet to change

        public CategoryRepository()
        {
            _connectionStringName = "ToDoList";
            _connectionDetails = ConfigurationManager.ConnectionStrings[_connectionStringName];
            _providerName = _connectionDetails.ProviderName;
            _connectionString = _connectionDetails.ConnectionString;
            _dbFactory = DbProviderFactories.GetFactory(_providerName);

            // Parameters
            _idParam = _dbFactory.CreateParameter();
            _idParam.DbType = DbType.Int32;
            _idParam.ParameterName = "@Id";
            _idParam.SourceColumn = "Id";

            _nameParam = _dbFactory.CreateParameter();
            _nameParam.DbType = DbType.String;
            _nameParam.ParameterName = "@CategoryName";
            _nameParam.SourceColumn = "CategoryName";

            _descriptionParam = _dbFactory.CreateParameter();
            _descriptionParam.DbType = DbType.String;
            _descriptionParam.ParameterName = "@CategoryDescription";
            _descriptionParam.SourceColumn = "CategoryDescription";

            //query templates
            _selectQuery = @"SELECT Id, CategoryName, CategoryDescription FROM Categories";
            _insertQuery = @"INSERT INTO Categories (CategoryName, CategoryDescription) VALUES (@CategoryName, @CategoryDescription)";
            _updateQuery = @"UPDATE Categories SET CategoryName = @CategoryName, CategoryDescription = @CategoryDescription WHERE Id = @Id";
            _deleteQuery = @"DELETE FROM Categories WHERE Id = @Id";

            _adapter = _dbFactory.CreateDataAdapter();
            _data = new DataSet();
            //init DataSet
            this.GetCategory(-1);
        }

        private readonly string _connectionStringName;
        private ConnectionStringSettings _connectionDetails;
        private string _providerName;
        private string _connectionString;
        private string _selectQuery;
        private string _insertQuery;
        private string _updateQuery;
        private string _deleteQuery;
        private DbParameter _idParam;
        private DbParameter _nameParam;
        private DbParameter _descriptionParam;
        private DbProviderFactory _dbFactory;
        private DbDataAdapter _adapter;

        private DataSet _data;

        public DataSet Data
        {
            get
            {
                return _data;
            }
            private set
            {
                _data = value;
                DataSetChanged?.Invoke();
            }
        }

        public void GetAllCategories()
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = _selectQuery;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                _adapter.Fill(_data);
            }
        }

        public void GetCategory(int id, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE Id = {id}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public void GetCategory(string categoryName, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE CategoryName = {categoryName}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public int AddNewCategory(string categoryName, string categoryDescription = null)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Configure Insert Command
                var insertCommand = _dbFactory.CreateCommand();
                insertCommand.Parameters.Add(_descriptionParam);
                insertCommand.Parameters.Add(_nameParam);
                insertCommand.Connection = connection;
                insertCommand.CommandType = CommandType.Text;
                insertCommand.CommandText = _insertQuery;

                // Create and configure a DbDataAdapter
                _adapter.InsertCommand = insertCommand;
                _data.Tables[0].Rows.Add(0, categoryName, categoryDescription);
                result = _adapter.Update(_data.Tables[0]);
            }
            return result;
        }

        public int UpdateCategory(int id, string newCategoryName, string newCategoryDescription = null)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetCategory(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0]["CategoryName"] = newCategoryName;
                    temporaryDataSet.Tables[0].Rows[0]["CategoryDescription"] = newCategoryDescription;
                }

                // Configure Update Command.
                var updateCommand = _dbFactory.CreateCommand();

                updateCommand.CommandText = _updateQuery;
                updateCommand.Parameters.Add(_idParam);
                updateCommand.Parameters.Add(_nameParam);
                updateCommand.Parameters.Add(_descriptionParam);

                updateCommand.Connection = connection;
                updateCommand.CommandType = CommandType.Text;
                _adapter.UpdateCommand = updateCommand;

                // Create and configure a DbDataAdapter
                _adapter.UpdateCommand = updateCommand;
                
                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public int DeleteCategory(int id)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetCategory(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0].Delete();
                }

                // Delete Command
                var deleteCommand = _dbFactory.CreateCommand();

                deleteCommand.CommandText = _deleteQuery;
                deleteCommand.Parameters.Add(_idParam);

                deleteCommand.Connection = connection;
                deleteCommand.CommandType = CommandType.Text;
                _adapter.DeleteCommand = deleteCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public event Action DataSetChanged;

    }
}
