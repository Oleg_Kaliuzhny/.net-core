﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace Data.Database.Domains
{
    public class CommentRepository
    {
        //TODO
        //INSERT, UPDATE, DELETE range and add param of DataSet to change

        public CommentRepository()
        {
            _connectionStringName = "ToDoList";
            _connectionDetails = ConfigurationManager.ConnectionStrings[_connectionStringName];
            _providerName = _connectionDetails.ProviderName;
            _connectionString = _connectionDetails.ConnectionString;
            _dbFactory = DbProviderFactories.GetFactory(_providerName);

            // Parameters
            _idParam = _dbFactory.CreateParameter();
            _idParam.DbType = DbType.Int32;
            _idParam.ParameterName = "@Id";
            _idParam.SourceColumn = "Id";

            _userIdParam = _dbFactory.CreateParameter();
            _userIdParam.DbType = DbType.Int32;
            _userIdParam.ParameterName = "@UserId";
            _userIdParam.SourceColumn = "UserId";

            _commentParam = _dbFactory.CreateParameter();
            _commentParam.DbType = DbType.String;
            _commentParam.ParameterName = "@Comment";
            _commentParam.SourceColumn = "Comment";

            _replyToParam = _dbFactory.CreateParameter();
            _replyToParam.DbType = DbType.Int32;
            _replyToParam.ParameterName = "@ReplyTo";
            _replyToParam.SourceColumn = "ReplyTo";

            _taskIdParam = _dbFactory.CreateParameter();
            _taskIdParam.DbType = DbType.Int32;
            _taskIdParam.ParameterName = "@TaskId";
            _taskIdParam.SourceColumn = "TaskId";

            //query templates
            _selectQuery = @"SELECT [Id], [UserId], [Comment], [ReplyTo], [TaskId] FROM Comments";
            _insertQuery = @"INSERT INTO Comments ([UserId], [Comment], [ReplyTo], [TaskId]) VALUES (@UserId, @Comment, @ReplyTo, @TaskId)";
            _updateQuery = @"UPDATE Comments SET UserId = @UserId, Comment = @Comment, ReplyTo = @ReplyTo, TaskId = @TaskId WHERE Id = @Id";
            _deleteQuery = @"DELETE FROM Comments WHERE Id = @Id";

            _adapter = _dbFactory.CreateDataAdapter();
            _data = new DataSet();
            //init DataSet
            this.GetComment(-1);
        }

        private readonly string _connectionStringName;
        private ConnectionStringSettings _connectionDetails;
        private string _providerName;
        private string _connectionString;
        private string _selectQuery;
        private string _insertQuery;
        private string _updateQuery;
        private string _deleteQuery;
        private DbParameter _idParam;
        private DbParameter _userIdParam;
        private DbParameter _commentParam;
        private DbParameter _replyToParam;
        private DbParameter _taskIdParam;
        private DbProviderFactory _dbFactory;
        private DbDataAdapter _adapter;

        private DataSet _data;

        public DataSet Data
        {
            get
            {
                return _data;
            }
            private set
            {
                _data = value;
                DataSetChanged?.Invoke();
            }
        }

        public void GetAllComments()
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = _selectQuery;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                _adapter.Fill(_data);
            }
        }

        public void GetComment(int id, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE Id = {id}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public void GetComments(int taskId, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE TaskId = {taskId}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public int AddNewComment(int userId, string comment, int? replyTo, int taskId)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Configure Insert Command
                var insertCommand = _dbFactory.CreateCommand();
                insertCommand.Parameters.Add(_commentParam);
                insertCommand.Parameters.Add(_userIdParam);
                insertCommand.Parameters.Add(_replyToParam);
                insertCommand.Parameters.Add(_taskIdParam);
                insertCommand.Connection = connection;
                insertCommand.CommandType = CommandType.Text;
                insertCommand.CommandText = _insertQuery;

                // Create and configure a DbDataAdapter
                _adapter.InsertCommand = insertCommand;
                _data.Tables[0].Rows.Add(0, userId, comment, replyTo, taskId);
                result = _adapter.Update(_data.Tables[0]);
            }
            return result;
        }

        public int UpdateComment(int id, int userId, string comment, int? replyTo, int taskId)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetComment(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0]["UserId"] = userId;
                    temporaryDataSet.Tables[0].Rows[0]["Comment"] = comment;
                    temporaryDataSet.Tables[0].Rows[0]["ReplyTo"] = comment;
                    temporaryDataSet.Tables[0].Rows[0]["TaskId"] = comment;
                }

                // Configure Update Command.
                var updateCommand = _dbFactory.CreateCommand();

                updateCommand.CommandText = _updateQuery;
                updateCommand.Parameters.Add(_idParam);
                updateCommand.Parameters.Add(_commentParam);
                updateCommand.Parameters.Add(_userIdParam);
                updateCommand.Parameters.Add(_replyToParam);
                updateCommand.Parameters.Add(_taskIdParam);

                updateCommand.Connection = connection;
                updateCommand.CommandType = CommandType.Text;
                _adapter.UpdateCommand = updateCommand;

                // Create and configure a DbDataAdapter
                _adapter.UpdateCommand = updateCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public int DeleteComment(int id)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetComment(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0].Delete();
                }

                // Delete Command
                var deleteCommand = _dbFactory.CreateCommand();

                deleteCommand.CommandText = _deleteQuery;
                deleteCommand.Parameters.Add(_idParam);

                deleteCommand.Connection = connection;
                deleteCommand.CommandType = CommandType.Text;
                _adapter.DeleteCommand = deleteCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public event Action DataSetChanged;
    }
}
