﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace Data.Database.Domains
{
    public class TaskRepository
    {
        //TODO
        //INSERT, UPDATE, DELETE range and add param of DataSet to change

        public TaskRepository()
        {
            _connectionStringName = "ToDoList";
            _connectionDetails = ConfigurationManager.ConnectionStrings[_connectionStringName];
            _providerName = _connectionDetails.ProviderName;
            _connectionString = _connectionDetails.ConnectionString;
            _dbFactory = DbProviderFactories.GetFactory(_providerName);

            // Parameters
            _idParam = _dbFactory.CreateParameter();
            _idParam.DbType = DbType.Int32;
            _idParam.ParameterName = "@Id";
            _idParam.SourceColumn = "Id";

            _userIdParam = _dbFactory.CreateParameter();
            _userIdParam.DbType = DbType.Int32;
            _userIdParam.ParameterName = "@UserId";
            _userIdParam.SourceColumn = "UserId";

            _titleParam = _dbFactory.CreateParameter();
            _titleParam.DbType = DbType.String;
            _titleParam.ParameterName = "@Title";
            _titleParam.SourceColumn = "Title";

            _descriptionParam = _dbFactory.CreateParameter();
            _descriptionParam.DbType = DbType.String;
            _descriptionParam.ParameterName = "@Description";
            _descriptionParam.SourceColumn = "Description";

            _priorityParam = _dbFactory.CreateParameter();
            _priorityParam.DbType = DbType.Int32;
            _priorityParam.ParameterName = "@Priority";
            _priorityParam.SourceColumn = "Priority";

            _statusIdParam = _dbFactory.CreateParameter();
            _statusIdParam.DbType = DbType.Int32;
            _statusIdParam.ParameterName = "@StatusId";
            _statusIdParam.SourceColumn = "StatusId";

            _creationDateParam = _dbFactory.CreateParameter();
            _creationDateParam.DbType = DbType.DateTime;
            _creationDateParam.ParameterName = "@CreationDate";
            _creationDateParam.SourceColumn = "CreationDate";

            _beginningDateParam = _dbFactory.CreateParameter();
            _beginningDateParam.DbType = DbType.DateTime;
            _beginningDateParam.ParameterName = "@BeginningDate";
            _beginningDateParam.SourceColumn = "BeginningDate";

            _finishingDateParam = _dbFactory.CreateParameter();
            _finishingDateParam.DbType = DbType.DateTime;
            _finishingDateParam.ParameterName = "@FinishingDate";
            _finishingDateParam.SourceColumn = "FinishingDate";

            _remindHourParam = _dbFactory.CreateParameter();
            _remindHourParam.DbType = DbType.Int16;
            _remindHourParam.ParameterName = "@RemindHour";
            _remindHourParam.SourceColumn = "RemindHour";

            _categoryIdParam = _dbFactory.CreateParameter();
            _categoryIdParam.DbType = DbType.Int32;
            _categoryIdParam.ParameterName = "@CategoryId";
            _categoryIdParam.SourceColumn = "CategoryId";

            _attributesParam = _dbFactory.CreateParameter();
            _attributesParam.DbType = DbType.Int32;
            _attributesParam.ParameterName = "@Attributes";
            _attributesParam.SourceColumn = "Attributes";

            //query templates
            _selectQuery = @"SELECT Id, UserId, Title, Description, Priority, StatusId, CreationDate, BeginningDate, 
FinishingDate, RemindHour, CategoryId, Attributes FROM Tasks";
            _insertQuery = @"INSERT INTO Tasks (UserId, Title, Description, Priority, StatusId, CreationDate, BeginningDate, 
FinishingDate, RemindHour, CategoryId, Attributes) VALUES (@UserId, @Title, @Description, @Priority, @StatusId, @CreationDate, 
@BeginningDate, @FinishingDate, @RemindHour, @CategoryId, @Attributes)";
            _updateQuery = @"UPDATE Tasks SET UserId = @UserId, Title = @Title, Description = @Description, 
Priority = @Priority, StatusId = @StatusId, CreationDate = @CreationDate, 
BeginningDate = @BeginningDate, FinishingDate = @FinishingDate, RemindHour = @RemindHour, CategoryId = @CategoryId, 
Attributes = @Attributes WHERE Id = @Id";
            _deleteQuery = @"DELETE FROM Tasks WHERE Id = @Id";

            _adapter = _dbFactory.CreateDataAdapter();
            _data = new DataSet();
            //init DataSet
            this.GetTask(-1);
        }

        private readonly string _connectionStringName;
        private ConnectionStringSettings _connectionDetails;
        private string _providerName;
        private string _connectionString;
        private string _selectQuery;
        private string _insertQuery;
        private string _updateQuery;
        private string _deleteQuery;
        private DbParameter _idParam;
        private DbParameter _userIdParam;
        private DbParameter _titleParam;
        private DbParameter _descriptionParam;
        private DbParameter _priorityParam;
        private DbParameter _statusIdParam;
        private DbParameter _creationDateParam;
        private DbParameter _beginningDateParam;
        private DbParameter _finishingDateParam;
        private DbParameter _remindHourParam;
        private DbParameter _categoryIdParam;
        private DbParameter _attributesParam;
        private DbProviderFactory _dbFactory;
        private DbDataAdapter _adapter;

        private DataSet _data;

        public DataSet Data
        {
            get
            {
                return _data;
            }
            private set
            {
                _data = value;
                DataSetChanged?.Invoke();
            }
        }

        public void GetAllTasks()
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = _selectQuery;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                _adapter.Fill(_data);
            }
        }

        public void GetTask(int id, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE Id = {id}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public void GetTasks(int userId, DataSet outDataSet = null)
        {
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Create and configure a DbCommand
                var selectCommand = _dbFactory.CreateCommand();
                selectCommand.CommandText = $"{_selectQuery}  WHERE UserId = {userId}"; ;
                selectCommand.Connection = connection;

                // Create and configure a DbDataAdapter
                _adapter.SelectCommand = selectCommand;

                if (outDataSet is null)
                {
                    _adapter.Fill(_data);
                }
                else
                {
                    _adapter.Fill(outDataSet);
                }
            }
        }

        public int AddNewTask(int userId, string title, string description, int priority, int? statusId,
            DateTime? creationDate, DateTime? beginningDate, DateTime? finishingDate, int? remindHour,
            int? categoryId, int? attributes)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                // Configure Insert Command
                var insertCommand = _dbFactory.CreateCommand();

                insertCommand.Parameters.Add(_userIdParam);
                insertCommand.Parameters.Add(_titleParam);
                insertCommand.Parameters.Add(_descriptionParam);
                insertCommand.Parameters.Add(_priorityParam);
                insertCommand.Parameters.Add(_statusIdParam);
                insertCommand.Parameters.Add(_creationDateParam);
                insertCommand.Parameters.Add(_beginningDateParam);
                insertCommand.Parameters.Add(_finishingDateParam);
                insertCommand.Parameters.Add(_remindHourParam);
                insertCommand.Parameters.Add(_categoryIdParam);
                insertCommand.Parameters.Add(_attributesParam);
                insertCommand.Connection = connection;
                insertCommand.CommandType = CommandType.Text;
                insertCommand.CommandText = _insertQuery;

                // Create and configure a DbDataAdapter
                _adapter.InsertCommand = insertCommand;
                _data.Tables[0].Rows.Add(0, userId, title, description, priority, statusId, creationDate, beginningDate, finishingDate,
                    remindHour, categoryId, attributes);
                result = _adapter.Update(_data.Tables[0]);
            }
            return result;
        }

        public int UpdateTask(int id, int userId, string title, string description, int priority, int? statusId,
            DateTime? creationDate, DateTime? beginningDate, DateTime? finishingDate, int? remindHour,
            int? categoryId, int? attributes)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetTask(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0]["UserId"] = userId;
                    temporaryDataSet.Tables[0].Rows[0]["Title"] = title;
                    temporaryDataSet.Tables[0].Rows[0]["Description"] = description;
                    temporaryDataSet.Tables[0].Rows[0]["Priority"] = priority;
                    temporaryDataSet.Tables[0].Rows[0]["StatusId"] = statusId;
                    temporaryDataSet.Tables[0].Rows[0]["CreationDate"] = creationDate;
                    temporaryDataSet.Tables[0].Rows[0]["BeginningDate"] = beginningDate;
                    temporaryDataSet.Tables[0].Rows[0]["FinishingDate"] = finishingDate;
                    temporaryDataSet.Tables[0].Rows[0]["RemindHour"] = remindHour;
                    temporaryDataSet.Tables[0].Rows[0]["CategoryId"] = categoryId;
                    temporaryDataSet.Tables[0].Rows[0]["Attributes"] = attributes;
                }

                // Configure Update Command.
                var updateCommand = _dbFactory.CreateCommand();

                updateCommand.CommandText = _updateQuery;
                updateCommand.Parameters.Add(_idParam);
                updateCommand.Parameters.Add(_userIdParam);
                updateCommand.Parameters.Add(_titleParam);
                updateCommand.Parameters.Add(_descriptionParam);
                updateCommand.Parameters.Add(_priorityParam);
                updateCommand.Parameters.Add(_statusIdParam);
                updateCommand.Parameters.Add(_creationDateParam);
                updateCommand.Parameters.Add(_beginningDateParam);
                updateCommand.Parameters.Add(_finishingDateParam);
                updateCommand.Parameters.Add(_remindHourParam);
                updateCommand.Parameters.Add(_categoryIdParam);
                updateCommand.Parameters.Add(_attributesParam);
                updateCommand.Parameters.Add(_descriptionParam);

                updateCommand.Connection = connection;
                updateCommand.CommandType = CommandType.Text;
                _adapter.UpdateCommand = updateCommand;

                // Create and configure a DbDataAdapter
                _adapter.UpdateCommand = updateCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public int DeleteTask(int id)
        {
            int result = 0;
            using (var connection = _dbFactory.CreateConnection())
            {
                // Create and configure a connection
                connection.ConnectionString = _connectionString;

                var temporaryDataSet = new DataSet();
                this.GetTask(id, temporaryDataSet);

                if (temporaryDataSet.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    temporaryDataSet.Tables[0].Rows[0].Delete();
                }

                // Delete Command
                var deleteCommand = _dbFactory.CreateCommand();

                deleteCommand.CommandText = _deleteQuery;
                deleteCommand.Parameters.Add(_idParam);

                deleteCommand.Connection = connection;
                deleteCommand.CommandType = CommandType.Text;
                _adapter.DeleteCommand = deleteCommand;

                result = _adapter.Update(temporaryDataSet.Tables[0]);
            }
            return result;
        }

        public event Action DataSetChanged;
    }
}
