﻿using System;
using System.Linq;
using System.Collections.Generic;
using Data.New.Contracts;
using Data.New;

namespace CompareTest
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 1
            //List<ILamp> lamps = new List<ILamp>();

            //Random random = new Random();
            //for (int index = 0; index < 25; index++)
            //{
            //    var item = new Lamp(random.Next(1000));
            //    lamps.Add(item);
            //    lamps.Add(item);
            //}

            //lamps.Sort();
            //foreach (var lamp in lamps.Distinct())
            //{
            //    Console.WriteLine($"Power: {(lamp as ILamp).Power}");
            //}
            #endregion

            Person[] peopleArray = new Person[3];
            peopleArray[0] = new Person("Ivan", "Ivanovich");
            peopleArray[1] = new Person("Andrey", "Ivanovich");
            peopleArray[2] = new Person("Yurich", "Ivanovich");

            People peopleList = new People(peopleArray);

            foreach (Person p in peopleList)
            {
                Console.WriteLine("{0} {1}", p.FirstName, p.LastName);
            }
            Console.ReadKey();
        }
    }
}
