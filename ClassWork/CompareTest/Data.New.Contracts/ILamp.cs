﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.New.Contracts
{
    public interface ILamp
    {
        int Power { get; set; }
    }
}
