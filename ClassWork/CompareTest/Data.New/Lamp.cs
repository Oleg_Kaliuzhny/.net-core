﻿using Data.New.Contracts;
using System;

namespace Data.New
{
    public class Lamp : IComparable, IEquatable<ILamp>, ILamp
    {
        public Lamp() : this(0)
        {

        }

        public Lamp(int power)
        {
            this._power = power;
        }

        private int _power;

        public int Power
        {
            get { return _power; }
            set { _power = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public static bool operator ==(Lamp lamp1, ILamp lamp2)
        {
            return lamp1.Power == lamp2.Power;
        }

        public static bool operator !=(Lamp lamp1, ILamp lamp2)
        {
            return !(lamp1 == lamp2);
        }

        public static bool operator ==(ILamp lamp1, Lamp lamp2)
        {
            return lamp1.Power == lamp2.Power;
        }

        public static bool operator !=(ILamp lamp1, Lamp lamp2)
        {
            return !(lamp1 == lamp2);
        }

        public static bool operator >=(Lamp lamp1, Lamp lamp2)
        {
            return lamp1.Power >= lamp2.Power;
        }

        public static bool operator <=(Lamp lamp1, Lamp lamp2)
        {
            return lamp1.Power <= lamp2.Power;
        }

        public static bool operator >(Lamp lamp1, Lamp lamp2)
        {
            return lamp1.Power > lamp2.Power;
        }

        public static bool operator <(Lamp lamp1, Lamp lamp2)
        {
            return lamp1.Power < lamp2.Power;
        }

        public bool Equals(ILamp other)
        {
            if (other is null)
            {
                return false;
            }
            return this.Power == other.Power;
        }

        public override bool Equals(object other)
        {
            if (other is null)
            {
                return false;
            }
            return Equals(other as Lamp);
        }

        public int CompareTo(object obj)
        {
            Lamp lamp = (Lamp)obj;
            if (lamp is null)
            {
                return 1;
            }
            if (this.Power > lamp.Power)
            {
                return 1;
            }
            if (this.Power < lamp.Power)
            {
                return -1;
            }
            return 0;
        }

        public override int GetHashCode()
        {
            return Power.GetHashCode();
        }
    }
}
