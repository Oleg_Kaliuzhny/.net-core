﻿using System;

namespace BL
{
    public class Triangle : Figure
    {
        public Triangle(double firstSide, double secondSide, double thirdSide)
        {
            _firstSide = firstSide;
            _secondSide = secondSide;
            _thirdSide = thirdSide;
        }

        private double _firstSide;
        private double _secondSide;
        private double _thirdSide;

        public override double Perimeter()
        {
            return _firstSide + _secondSide + _thirdSide;
        }

        public override double Square()
        {
            double halfSquare = (double)this.Square() / 2;
            return Math.Sqrt(halfSquare * (halfSquare - _firstSide) * (halfSquare - _secondSide) * (halfSquare - _thirdSide));
        }
    }
}
