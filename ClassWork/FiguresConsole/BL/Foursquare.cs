﻿namespace BL
{
    public class Foursquare : Figure
    {
        public Foursquare(double firstSide, double secondSide)
        {
            _firstSide = firstSide;
            _secondSide = secondSide;
        }

        private double _firstSide;
        private double _secondSide;

        public override double Perimeter()
        {
            return (_firstSide + _secondSide) * 2;
        }

        public override double Square()
        {
            return _firstSide * _secondSide;
        }
    }
}
