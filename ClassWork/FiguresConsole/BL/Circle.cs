﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Circle : Figure
    {
        public Circle(double radius)
        {
            _radius = radius;
        }

        private double _radius;

        public override double Perimeter()
        {
            return 2 * Math.PI * _radius;
        }

        public override double Square()
        {
            return Math.PI * Math.Pow(_radius, 2);
        }
    }
}
