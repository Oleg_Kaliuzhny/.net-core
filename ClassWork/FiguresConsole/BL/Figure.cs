﻿namespace BL
{
    public abstract class Figure
    {
        public abstract double Perimeter();

        public abstract double Square();
    }
}
