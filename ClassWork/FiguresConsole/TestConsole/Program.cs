﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TestConsole
{
    class Program
    {
        public static void Main(string[] args)
        {
            IPerson pers = new Person("lol", 12);

            List<IPerson> list = new List<IPerson>();
            list.Add(pers);

            // передаем в конструктор тип класса
            XmlSerializer formatter = new XmlSerializer(typeof(List<IPerson>));

            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("persons.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, list);

                Console.WriteLine("Объект сериализован");
            }

        }
    }

    public interface IPerson
    {
        string Name { get; set; }
        int Age { get; set; }
    }

    [Serializable]
    public class Person : IPerson
    {
        public string Name { get; set; }
        public int Age { get; set; }

        // стандартный конструктор без параметров
        public Person()
        { }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}
