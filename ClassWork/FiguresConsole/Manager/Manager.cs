﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace Manager
{
    public class Manager
    {
        public enum Figures
        {
            Circle,
            Triangle,
            Foursquare
        }

        private Figure _figure;

        public void SetFigureCircle(int radius)
        {
            _figure = new Circle(radius);
        }

        public void SetFigureFoursquare(int a, int b)
        {
            _figure = new Foursquare(a, b);
        }

        public void SetFigureTriangle(int a, int b, int c)
        {
            _figure = new Triangle(a, b, c);
        }

        public double GetPerimeter()
        {
            if (_figure == null)
                return -1;
            return _figure.Perimeter();
        }

        public double GetSquare()
        {
            if (_figure == null)
                return -1;
            return _figure.Square();
        }
    }
}
