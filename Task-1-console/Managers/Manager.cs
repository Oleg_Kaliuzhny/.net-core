﻿using System;
using Data;
using Data.Contracts;

namespace Managers
{
    public class Manager
    {
        public Manager(string[] args)
        {
            ParseArgs(args);
            if (!_detectedError)
            {
                _reader = new Reader(_inputFile);
                _sorting = new Sorting();
                _writer = new Writer(_outputFile);
                Process();
            }
            else
            {
                Console.WriteLine("Ошибка в аргументах. Выходим...");
            }
        }

        enum SortingMethod
        {
            QuickSort,
            ShellSort,
            BubbleSort,
            SelectionSort,
            InsertionSort,
            Nothing
        };

        private string[] _text;
        private string _inputFile = null;
        private string _outputFile = null;
        private SortingMethod _sortingMethod = SortingMethod.Nothing;
        private bool _detectedError = false;
        private IReader _reader;
        private IWriter _writer;
        private ISorting _sorting;
        private bool _ascending = true;

        private void ParseArgs(string[] args)
        {
            bool needsContinue = false;
            for (int i = 0; i < args.Length; i++)
            {
                if (needsContinue)
                {
                    needsContinue = false;
                    continue;
                }
                switch (args[i])
                {
                    case "/qs":
                        if (_sortingMethod != SortingMethod.Nothing)
                            _detectedError = true;
                        _sortingMethod = SortingMethod.QuickSort;
                        break;
                    case "/shs":
                        if (_sortingMethod != SortingMethod.Nothing)
                            _detectedError = true;
                        _sortingMethod = SortingMethod.ShellSort;
                        break;
                    case "/ses":
                        if (_sortingMethod != SortingMethod.Nothing)
                            _detectedError = true;
                        _sortingMethod = SortingMethod.SelectionSort;
                        break;
                    case "/bs":
                        if (_sortingMethod != SortingMethod.Nothing)
                            _detectedError = true;
                        _sortingMethod = SortingMethod.BubbleSort;
                        break;
                    case "/is":
                        if (_sortingMethod != SortingMethod.Nothing)
                            _detectedError = true;
                        _sortingMethod = SortingMethod.BubbleSort;
                        break;
                    case "-i":
                        if (i + 1 >= args.Length)
                            _detectedError = true;
                        _inputFile = args[i + 1];
                        needsContinue = true;
                        break;
                    case "-o":
                        if (i + 1 >= args.Length)
                            _detectedError = true;
                        _outputFile = args[i + 1];
                        needsContinue = true;
                        break;
                    case "--descending":
                        _ascending = false;
                        break;
                }
            }
        }

        private void Process()
        {
            _text = _reader.ReadAll();
            if (_text == null)
                return;

            switch (_sortingMethod)
            {
                case SortingMethod.QuickSort:
                    _sorting.QuickSort<string>(_text, _ascending);
                    break;
                case SortingMethod.ShellSort:
                    _sorting.ShellSort<string>(_text, _ascending);
                    break;
                case SortingMethod.BubbleSort:
                    _sorting.BubbleSort<string>(_text, _ascending);
                    break;
                case SortingMethod.SelectionSort:
                    _sorting.SelectionSort<string>(_text, _ascending);
                    break;
                case SortingMethod.InsertionSort:
                    _sorting.InsertionSort<string>(_text, _ascending);
                    break;
                default:
                    break;
            }

            _writer.WriteAll(_text);
        }
    }
}
