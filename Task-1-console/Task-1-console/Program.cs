﻿using System;
using Managers;

namespace Task_1_console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
                Helper();
            else if (args[0] == "/?")
                Helper();
            else
                new Manager(args);

#if DEBUG
            Console.ReadKey();
#endif
        }

        static void Helper()
        {
            Console.WriteLine("Шаблон запуска:");
            Console.WriteLine("sorting.exe [метод_сортировки] [ключи]");
            Console.WriteLine("");
            Console.WriteLine("Методы сортировки:");
            Console.WriteLine("\t /qs - быстрая сортировка");
            Console.WriteLine("\t /shs - сортировка Шелла");
            Console.WriteLine("\t /bs - сортировка пузырьком");
            Console.WriteLine("\t /ses - сортировка выбором");
            Console.WriteLine("\t /is - сортировка вставками");
            Console.WriteLine("");
            Console.WriteLine("Ключи:");
            Console.WriteLine("\t/? - справка");
            Console.WriteLine("\t-i [файл] - входной файл");
            Console.WriteLine("\t-o [файл] - выходной файл");
            Console.WriteLine("\t--descending - сортировать по убыванию");
            Console.WriteLine("");
            Console.WriteLine("При отсутствиии ключа \"-i\" и/или \"-o\" будет использоваться стандартный поток ввода/вывода");
            Console.WriteLine("При использовании стандартного потока ввода окончанием считывания является пустая строка");
            Console.WriteLine("");
            Console.WriteLine("Примеры запуска:");
            Console.WriteLine("sorting.exe /?");
            Console.WriteLine("sorting.exe /qs -i sort-me.txt -o sorted.txt");
        }
    }
}
