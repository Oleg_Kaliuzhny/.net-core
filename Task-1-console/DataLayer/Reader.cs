﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Data.Contracts;

namespace Data
{
    public class Reader : IReader
    {
        public Reader(string filepath = null)
        {
            _filepath = filepath;
        }

        private string _filepath;
        
        public string[] ReadAll()
        {
            if (_filepath != null)
            {
                if (!File.Exists(_filepath))
                {
                    Console.WriteLine("Файл {0} не существует!", _filepath);
                    return null;
                }
                else if (new FileInfo(_filepath).Length == 0)
                {
                    Console.WriteLine("Файл {0} пустой!", _filepath);
                    return null;
                }
                return File.ReadAllLines(_filepath, Encoding.Default);
            }
            else
                return ReadConsole();
        }

        private string[] ReadConsole()
        {
            List<string> list = new List<string>();
            string str;
            while (true)
            {
                str = Console.ReadLine();
                if (string.IsNullOrEmpty(str))
                    break;
                list.Add(str);
            }
            return list.ToArray();
        }
    }
}
