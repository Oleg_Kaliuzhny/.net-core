﻿using System;
using Data.Contracts;

namespace Data
{
    public class Sorting : ISorting
    {
        #region QuickSort
        public void QuickSort<T>(T[] array, bool ascending = true) where T : IComparable
        {
            quickSort(array, 0, array.Length - 1, ascending);
        }

        private void quickSort<T>(T[] array, int first, int last, bool ascending) where T : IComparable
        {
            if (first >= last)
                return;
            int c = partition(array, first, last, ascending);
            quickSort(array, first, c - 1, ascending);
            quickSort(array, c + 1, last, ascending);
        }

        private int partition<T>(T[] array, int first, int last, bool ascending) where T : IComparable
        {
            int i = first;
            for (int j = first; j <= last; j++)
            {
                if (ascending)
                {
                    if (array[j].CompareTo(array[last]) <= 0)
                    {
                        T temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                        i++;
                    }
                }
                else
                {
                    if (array[j].CompareTo(array[last]) >= 0)
                    {
                        T temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                        i++;
                    }
                }
            }
            return i - 1;
        }
        #endregion

        #region BubbleSort
        public void BubbleSort<T>(T[] array, bool ascending = true) where T : IComparable
        {
            for (int i = 0; i < array.Length - 1; i++)
                for (int j = 0; j < array.Length - i - 1; j++)
                    if (ascending)
                    {
                        if (array[j].CompareTo(array[j + 1]) > 0)
                        {
                            T t = array[j];
                            array[j] = array[j + 1];
                            array[j + 1] = t;
                        }
                    }
                    else
                    {
                        if (array[j].CompareTo(array[j + 1]) < 0)
                        {
                            T t = array[j];
                            array[j] = array[j + 1];
                            array[j + 1] = t;
                        }
                    }
        }
        #endregion

        #region ShellSort
        public void ShellSort<T>(T[] array, bool ascending = true) where T : IComparable
        {
            int step = array.Length / 2;
            if (ascending)
            {
                while (step > 0)
                {
                    for (int i = 0; i < (array.Length - step); i++)
                    {
                        int j = i;
                        while ((j >= 0) && (array[j].CompareTo(array[j + step]) > 0))
                        {
                            T tmp = array[j];
                            array[j] = array[j + step];
                            array[j + step] = tmp;
                            j -= step;
                        }
                    }
                    step = step / 2;
                }
            }
            else
            {
                while (step > 0)
                {
                    for (int i = 0; i < (array.Length - step); i++)
                    {
                        int j = i;
                        while ((j >= 0) && (array[j].CompareTo(array[j + step]) < 0))
                        {
                            T tmp = array[j];
                            array[j] = array[j + step];
                            array[j + step] = tmp;
                            j -= step;
                        }
                    }
                    step = step / 2;
                }
            }
        }
        #endregion

        #region SelectionSort
        public void SelectionSort<T>(T[] array, bool ascending = true) where T : IComparable
        {
            for (int i = 0; i < array.Length; i++)
            {
                int min = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (ascending)
                    {
                        if (array[j].CompareTo(array[min]) < 0)
                        {
                            min = j;
                        }
                    }
                    else
                    {
                        if (array[j].CompareTo(array[min]) > 0)
                        {
                            min = j;
                        }
                    }
                }

                //swap
                T temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
        }
        #endregion

        #region InsertionSort
        public void InsertionSort<T>(T[] array, bool ascending = true) where T : IComparable
        {
            for (int i = 1; i < array.Length; i++)
            {
                T temp = array[i];
                int j = i;
                if (ascending)
                    while (j > 0 && temp.CompareTo(array[j - 1]) < 0)
                    {
                        array[j] = array[j - 1];
                        j--;
                    }
                else
                    while (j > 0 && temp.CompareTo(array[j - 1]) > 0)
                    {
                        array[j] = array[j - 1];
                        j--;
                    }
                array[j] = temp;
            }
        }
        #endregion
    }
}
