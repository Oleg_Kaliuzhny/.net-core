﻿using System;
using System.IO;
using System.Text;
using Data.Contracts;

namespace Data
{
    public class Writer : IWriter
    {
        public Writer(string filepath = null)
        {
            _filepath = filepath;
        }

        private string _filepath;

        public void WriteAll(string[] array)
        {
            if (_filepath != null)
            {
                StreamWriter streamWriter = new StreamWriter(_filepath, false, Encoding.Default);
                foreach (var item in array)
                {
                    streamWriter.WriteLine(item);
                }
                streamWriter.Dispose();
            }
            else
                WriteAllToConsole(array);
        }

        private void WriteAllToConsole(string[] array)
        {
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }
    }
}
