﻿namespace Data.Contracts
{
    public interface IReader
    {
        string[] ReadAll();
    }
}