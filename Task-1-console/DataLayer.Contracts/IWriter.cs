﻿namespace Data.Contracts
{
    public interface IWriter
    {
        void WriteAll(string[] array);
    }
}