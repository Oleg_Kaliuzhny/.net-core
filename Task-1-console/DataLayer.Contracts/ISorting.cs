﻿using System;

namespace Data.Contracts
{
    public interface ISorting
    {
        void BubbleSort<T>(T[] array, bool ascending = true) where T : IComparable;
        void QuickSort<T>(T[] array, bool ascending = true) where T : IComparable;
        void ShellSort<T>(T[] array, bool ascending = true) where T : IComparable;
        void SelectionSort<T>(T[] array, bool ascending = true) where T : IComparable;
        void InsertionSort<T>(T[] array, bool ascending = true) where T : IComparable;
    }
}