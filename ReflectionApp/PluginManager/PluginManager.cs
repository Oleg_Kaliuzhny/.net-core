﻿using System.Collections.Generic;
using PluginManager.Contracts;
using System.Reflection;
using System.IO;
using System;
using Plugins.Interfaces;

namespace PluginManager
{
    public class PluginManager : IPluginManager
    {
        public PluginManager()
        {
            _plugins = new List<IPlugin>();
        }

        private List<IPlugin> _plugins;

        public bool ImportPlugin(string path)
        {
            if (!File.Exists(path))
                return false;

            IPlugin plugin = null;
            var asm = Assembly.LoadFile(path);
            var types = asm.GetTypes();
            foreach (var type in types)
            {
                var typeInfo = type.GetTypeInfo();
                foreach (var item in typeInfo.ImplementedInterfaces)
                {
                    if (item.Name == typeof(IPlugin).GetTypeInfo().Name)
                        plugin = Activator.CreateInstance(type) as IPlugin; 
                }
            }

            if (plugin is null)
                return false;
            else
                _plugins.Add(plugin);
            
            return true;
        }

        public double ExecutePlugin(int index, double arg1, double arg2)
        {
            return _plugins[index].Execute(arg1, arg2);
        }

        public List<string> GetDescriptions()
        {
            var descriptions = new List<string>(_plugins.Count);
            for (int i = 0; i < _plugins.Count; i++)
            {
                descriptions.Add(_plugins[i].GetExecuteDescription());
            }
            return descriptions;
        }
    }
}
