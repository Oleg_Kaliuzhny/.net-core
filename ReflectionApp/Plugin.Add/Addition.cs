﻿using Plugins.Interfaces;

namespace Plugin.Add
{
    public class Addition : IPlugin
    {
        public double Execute(double arg1, double arg2)
        {
            return arg1 + arg2;
        }

        public string GetExecuteDescription()
        {
            return "Sum of two numbers.";
        }
    }
}
