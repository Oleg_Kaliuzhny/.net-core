﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.Interfaces
{
    public interface IPlugin
    {
        double Execute(double arg1, double arg2);
        string GetExecuteDescription();
    }
}
