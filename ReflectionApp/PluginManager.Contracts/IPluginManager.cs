﻿using System.Collections.Generic;

namespace PluginManager.Contracts
{
    public interface IPluginManager
    {
        double ExecutePlugin(int index, double arg1, double arg2);
        List<string> GetDescriptions();
        bool ImportPlugin(string path);
    }
}