﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PluginManager.PluginManager plugins = new PluginManager.PluginManager();
            DisplayManager displayManager = new DisplayManager(plugins);
            

#if DEBUG
            Console.WriteLine("Finished");
            Console.ReadKey();
#endif
        }
    }
}
