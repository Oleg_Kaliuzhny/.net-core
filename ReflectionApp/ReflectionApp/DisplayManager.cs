﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReflectionApp
{
    public class DisplayManager
    {
        public DisplayManager(PluginManager.Contracts.IPluginManager pluginManager)
        {
            _plugins = pluginManager;
            this.ShowMenu();
        }

        PluginManager.Contracts.IPluginManager _plugins;

        void ShowDlls()
        {
            var files = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*.dll");
            Console.WriteLine("Dll's in the current folder:");
            foreach (var file in files)
            {
                Console.WriteLine(file);
            }
        }

        void ReadPath()
        {
            this.ShowDlls();
            Console.WriteLine("Dll to load:");
            var path = Console.ReadLine();

            if (File.Exists(path))
            {
                if(_plugins.ImportPlugin(path))
                {
                    Console.WriteLine("Loaded.");
                }
                else
                {
                    Console.WriteLine("Loading error.");
                }
            }
            else
            {
                Console.WriteLine("File doesn't exist!");
            }
        }

        void ShowFunctions()
        {
            var descriptions = _plugins.GetDescriptions();
            Console.WriteLine("Dll ID : Description");
            for (var i = 0; i < descriptions.Count; i++)
            {
                Console.WriteLine($"{i}: {descriptions[i]}");
            }
        }

        void ShowExecutionDialog()
        {
            Console.Write("Dll ID: ");
            var dllIdStr = Console.ReadLine();
            var dllId = -1;
            if (!int.TryParse(dllIdStr, out dllId))
            {
                Console.WriteLine("It's not a number.");
                return;
            }
            else if (dllId > _plugins.GetDescriptions().Count)
            {
                Console.WriteLine("ID is out of range.");
                return;
            }

            Console.Write("First arg: ");
            var argStr = Console.ReadLine();
            var arg1 = 0.0;
            if (!double.TryParse(argStr, out arg1))
            {
                Console.WriteLine("Parse error.");
                return;
            }

            Console.Write("First arg: ");
            argStr = Console.ReadLine();
            var arg2 = 0.0;
            if (!double.TryParse(argStr, out arg2))
            {
                Console.WriteLine("Parse error.");
                return;
            }

            Console.WriteLine("Result is {0}", _plugins.ExecutePlugin(dllId, arg1, arg2));
        }

        public void ShowMenu()
        {
            while (true)
            {
                Console.WriteLine("***Menu***");
                Console.WriteLine("1 - Load dll");
                Console.WriteLine("2 - Show functions");
                Console.WriteLine("3 - Execute");

                Console.WriteLine("Another - Exit");

                var answer = Console.ReadLine();
                if (answer == "1")
                {
                    this.ReadPath();
                }
                else if (answer == "2")
                {
                    this.ShowFunctions();
                }
                else if (answer == "3")
                {
                    this.ShowExecutionDialog();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}
