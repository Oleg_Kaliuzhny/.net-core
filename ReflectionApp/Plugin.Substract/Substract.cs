﻿namespace Plugin.Substract
{
    public class Substract : Plugins.Interfaces.IPlugin
    {
        public double Execute(double arg1, double arg2)
        {
            return arg1 - arg2;
        }

        public string GetExecuteDescription()
        {
            return "Difference of two numbers.";
        }
    }
}
