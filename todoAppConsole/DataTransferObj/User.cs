﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Data.Interfaces
{
    [Serializable]
    public class User
    { 
        public User() : this("", "", "null@null.com", 0, new List<TaskToDo>()) //initilization
        {

        }

        public User(string firstName, string lastName, string email, int passwordHash, List<TaskToDo> tasks)
        {
            _firstName = firstName;
            _lastName = lastName;
            _email = email;
            _passwordHash = passwordHash;
            _tasks = tasks;
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set
            {
                Regex regex = new Regex(@"\w{1,}@[a-z]{2,}.{2,}"); // reg exp for email
                if (regex.Match(value).Success)
                    _email = value;
            }
        }

        private int _passwordHash;

        public int PasswordHash
        {
            get { return _passwordHash; }
            set { _passwordHash = value; }
        }

        private List<TaskToDo> _tasks;

        public List<TaskToDo> Tasks
        {
            get { return _tasks; }
            set { _tasks = value; }
        }

        public static bool operator ==(User user1, User user2)
        {
            if (user1 is null || user2 is null)
                return false;
            return user1.Email == user2.Email;
        }

        public static bool operator !=(User user1, User user2)
        {
            if (user1 == null || user2 == null)
                return true;
            return user1.Email != user2.Email;
        }
    }
}
