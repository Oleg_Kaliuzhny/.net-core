﻿using System;

namespace Data.Interfaces
{
    [Serializable]
    public class TaskComment
    {
        public TaskComment() : this("null@null.com", "", DateTime.Now) //initialization
        {

        }

        public TaskComment(string userEmail, string comment, DateTime creationTime)
        {
            _userEmail = userEmail;
            _comment = comment;
            _commentTime = creationTime;
        }

        //user
        private string _userEmail;

        public string UserEmail
        {
            get { return _userEmail; }
            set { _userEmail = value; }
        }

        //comment
        private string _comment;

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        //time
        private DateTime _commentTime;

        public DateTime CommentTime
        {
            get { return _commentTime; }
            set { _commentTime = value; }
        }

        public static bool operator ==(TaskComment task1, TaskComment task2)
        {
            if (task1.Comment != task2.Comment)
            {
                return false;
            }

            if (task1.CommentTime != task2.CommentTime)
            {
                return false;
            }

            if (task1.UserEmail != task2.UserEmail)
            {
                return false;
            }

            return true;
        }

        public static bool operator !=(TaskComment task1, TaskComment task2)
        {
            return !(task1 == task2);
        }

    }
}
