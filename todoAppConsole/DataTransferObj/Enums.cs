﻿namespace Data.Interfaces
{
    public static class Enums
    {
        public enum TaskStatuses
        {
            Planned,
            Processing,
            Deferred,
            Done,
            Rejected
        }

        public enum TaskAttributes
        {
            Important,
            Hidden,
            None
        }
    }
}
