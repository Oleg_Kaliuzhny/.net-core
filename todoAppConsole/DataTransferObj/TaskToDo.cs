﻿using System;
using System.Collections.Generic;

namespace Data.Interfaces
{
    [Serializable]
    public class TaskToDo
    {
        public TaskToDo() : this("", "", 0, Enums.TaskStatuses.Planned, DateTime.Now,
            DateTime.Now, DateTime.Now, new List<DateTime>(), new List<TaskComment>(), new List<Enums.TaskAttributes>(), "None") // initilization
        {

        }

        public TaskToDo(string title, string description, int priority,
            Enums.TaskStatuses status, DateTime dateOfCreation,
            DateTime beginningDate, DateTime dateOfFinish, List<DateTime> notificationSchedule,
            List<TaskComment> comments, List<Enums.TaskAttributes> attributes, string category)
        {
            _title = title;
            _description = description;
            _priority = priority;
            _taskStatus = status;
            _dateOfCreation = dateOfCreation;
            _beginningDate = beginningDate;
            _dateOfFinish = dateOfFinish;
            _notificationSchedule = notificationSchedule;
            _comments = comments;
            _attributes = attributes;
            _category = category;
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _priority;

        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private Enums.TaskStatuses _taskStatus;

        public Enums.TaskStatuses TaskStatus
        {
            get { return _taskStatus; }
            set { _taskStatus = value; }
        }

        private DateTime _dateOfCreation;

        public DateTime DateOfCreation
        {
            get { return _dateOfCreation; }
            set { _dateOfCreation = value; }
        }

        private DateTime _beginningDate;

        public DateTime BeginningDate
        {
            get { return _beginningDate; }
            set { _beginningDate = value; }
        }

        private DateTime _dateOfFinish;

        public DateTime DateOfFinish
        {
            get { return _dateOfFinish; }
            set { _dateOfFinish = value; }
        }

        private List<DateTime> _notificationSchedule;

        public List<DateTime> NotificationSchedule
        {
            get { return _notificationSchedule; }
            set { _notificationSchedule = value; }
        }

        private List<TaskComment> _comments;

        public List<TaskComment> Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private List<Enums.TaskAttributes> _attributes;

        public List<Enums.TaskAttributes> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }

        private string _category;

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public override string ToString()
        {
            return _title.ToString();
        }
    }
}
