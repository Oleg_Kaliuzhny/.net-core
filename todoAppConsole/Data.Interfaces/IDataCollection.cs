﻿using System;
using System.Collections.Generic;

namespace Data.Interfaces
{
    public interface IDataCollection
    {
        bool AddTaskComment(string comment, string commentatorEmail, string commentatorPassword, string taskOwnerEmail, TaskToDo taskToComment, DateTime creationTime);
        bool AddUser(User user);
        bool AddUsersTask(string email, string password, TaskToDo newTask);
        bool AddUsersTask(User user, TaskToDo newTask);
        bool DeleteTaskComment(TaskComment commentToDelete, string commentOwnerEmail, string commentOwnerPassword, string taskOwnerEmail);
        bool DeleteUser(string email, string password);
        bool DeleteUser(User user);
        bool DeleteUsersTask(string userEmail, string userPassword, TaskToDo taskToDelete);
        User GetUser(string email, string password);
        List<TaskToDo> GetUsersTasks(string email);
        bool Load();
        bool Save();
    }
}