﻿using Data.Interfaces;
using Ninject;

namespace IoC
{
    public class Container
    {
        public Container()
        {
            _kernel = new StandardKernel(new DataBindingModule());
        }

        private IKernel _kernel;
 
        public Managers.DataManager GetDataManager()
        {
            return new Managers.DataManager(_kernel.Get<IDataCollection>());
        }
    }
}
