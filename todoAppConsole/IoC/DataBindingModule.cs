﻿using Data;
using Data.Interfaces;

namespace IoC
{
    class DataBindingModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IDataCollection>().To<DataCollection>();
        }
    }
}
