﻿using System;
using System.Collections.Generic;
using Data.Interfaces;

namespace IoC.Managers
{
    public class DataManager
    {
        private readonly IDataCollection _dataCollection;

        public DataManager(IDataCollection dataCollection)
        {
            _dataCollection = dataCollection;
        }

        public bool AddTaskComment(string comment, string commentatorEmail, string commentatorPassword, string taskOwnerEmail, TaskToDo taskToComment, DateTime creationTime)
        {
            return _dataCollection.AddTaskComment(comment, commentatorEmail, commentatorPassword, taskOwnerEmail, taskToComment, creationTime);
        }

        public bool AddUser(User user)
        {
            return _dataCollection.AddUser(user);
        }

        public bool AddUsersTask(string email, string password, TaskToDo newTask)
        {
            return _dataCollection.AddUsersTask(email, password, newTask);
        }

        public bool AddUsersTask(User user, TaskToDo newTask)
        {
            return _dataCollection.AddUsersTask(user, newTask);
        }

        public bool DeleteTaskComment(TaskComment commentToDelete, string commentOwnerEmail, string commentOwnerPassword, string taskOwnerEmail)
        {
            return _dataCollection.DeleteTaskComment(commentToDelete, commentOwnerEmail, commentOwnerPassword, taskOwnerEmail);
        }

        public bool DeleteUser(string email, string password)
        {
            return _dataCollection.DeleteUser(email, password);
        }

        public bool DeleteUser(User user)
        {
            return _dataCollection.DeleteUser(user);
        }

        public bool DeleteUsersTask(string userEmail, string userPassword, TaskToDo taskToDelete)
        {
            return _dataCollection.DeleteUsersTask(userEmail, userPassword, taskToDelete);
        }

        public User GetUser(string email, string password)
        {
            return _dataCollection.GetUser(email, password);
        }

        public List<TaskToDo> GetUsersTasks(string email)
        {
            return _dataCollection.GetUsersTasks(email);
        }

        public bool Load()
        {
            return _dataCollection.Load();
        }

        public bool Save()
        {
            return _dataCollection.Save();
        }
    }
}
