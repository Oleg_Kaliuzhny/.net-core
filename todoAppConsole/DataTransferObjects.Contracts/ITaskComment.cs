﻿using System;

namespace DTO.Interfaces
{
    public interface ITaskComment
    {
        string Comment { get; set; }
        DateTime CommentTime { get; set; }
        string UserEmail { get; set; }
    }
}