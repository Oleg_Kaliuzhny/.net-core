﻿using System;
using System.Collections.Generic;

namespace DTO.Interfaces
{
    public interface ITaskToDo
    {
        List<Enums.TaskAttributes> Attributes { get; set; }
        DateTime BeginningDate { get; set; }
        string Category { get; set; }
        List<ITaskComment> Comments { get; set; }
        DateTime DateOfCreation { get; set; }
        DateTime DateOfFinish { get; set; }
        string Description { get; set; }
        List<DateTime> NotificationSchedule { get; set; }
        int Priority { get; set; }
        Enums.TaskStatuses TaskStatus { get; set; }
        string Title { get; set; }

        string ToString();
    }
}