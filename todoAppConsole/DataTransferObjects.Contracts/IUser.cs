﻿using System.Collections.Generic;

namespace DTO.Interfaces
{
    public interface IUser
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        int PasswordHash { get; set; }
        List<TaskToDo> Tasks { get; set; }
    }
}