﻿using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Data
{
    public class DataCollection : IDataCollection
    {
        public DataCollection()
        {
            _usersList = new List<User>();
            _formatter = new XmlSerializer(typeof(List<User>));
        }

        private List<User> _usersList;
        private XmlSerializer _formatter;

        /// <summary>
        /// Save to XML
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (File.Exists("users.xml"))
                File.Delete("users.xml");
            using (FileStream fs = new FileStream("users.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    _formatter.Serialize(fs, _usersList);
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Load from XML
        /// </summary>
        /// <returns></returns>
        public bool Load()
        {
            if (!File.Exists("users.xml"))
                return false;

            using (FileStream fs = new FileStream("users.xml", FileMode.Open))
            {
                _usersList = (List<User>)_formatter.Deserialize(fs);
            }
            return true;
        }

        /// <summary>
        /// Adds a user to user list
        /// </summary>
        /// <param name="user">New user</param>
        /// <returns>Is successfully</returns>
        public bool AddUser(User user)
        {
            if (_usersList.Exists((x) => x == user))
                return false;
            _usersList.Add(user);
            return true;
        }

        /// <summary>
        /// Removes user from the list
        /// </summary>
        /// <param name="user">User to delete</param>
        /// <returns>Is successfully</returns>
        public bool DeleteUser(User user)
        {
            if (_usersList.Exists((x) => x == user))
            {
                var finder = _usersList.Find((x) => x == user);
                _usersList.Remove(finder);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes user from the list
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <returns>Is successfully</returns>
        public bool DeleteUser(string email, string password)
        {
            if (_usersList.Exists((x) => x.Email == email))
            {
                var finder = _usersList.Find((x) => x.Email == email);
                if (finder.PasswordHash == password.GetHashCode())
                {
                    return _usersList.Remove(finder);
                }
            }

            return false;
        }

        /// <summary>
        /// Gets user from the list
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <returns>Existing user from the list or null if it doesn't exist</returns>
        public User GetUser(string email, string password)
        {
            if (_usersList.Exists((x) => x.Email == email))
            {
                User user = _usersList.Find((x) => x.Email == email);
                if (user.PasswordHash == password.GetHashCode())
                {
                    return user;
                }
            }
            return null;
        }

        /// <summary>
        /// Adds user's task
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <param name="newTask">New task to add</param>
        /// <returns>Is successfully</returns>
        public bool AddUsersTask(string email, string password, TaskToDo newTask)
        {
            if (newTask == null)
                return false;

            if (_usersList.Exists((x) => x.Email == email))
            {
                User user = _usersList.Find((x) => x.Email == email);
                if (user.PasswordHash == password.GetHashCode())
                {
                    user.Tasks.Add(newTask);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Adds user's task
        /// </summary>
        /// <param name="user">User </param>
        /// <param name="newTask">New task to add</param>
        /// <returns>Is successfully</returns>
        public bool AddUsersTask(User user, TaskToDo newTask)
        {
            if (newTask is null)
                return false;

            if (_usersList.Exists((x) => x == user))
            {
                User currentUser = _usersList.Find((x) => x == user);
                currentUser.Tasks.Add(newTask);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes user's task
        /// </summary>
        /// <param name="userEmail">User email</param>
        /// <param name="userPassword">User password</param>
        /// <param name="taskToDelete">Task to remove</param>
        /// <returns>Is successfully</returns>
        public bool DeleteUsersTask(string userEmail, string userPassword, TaskToDo taskToDelete)
        {
            if (taskToDelete == null)
                return false;

            if (_usersList.Exists((x) => x.Email == userEmail))
            {
                User user = _usersList.Find((x) => x.Email == userEmail);
                if (user.PasswordHash == userPassword.GetHashCode())
                {
                    return user.Tasks.Remove(taskToDelete);
                }
            }
            return false;
        }

        /// <summary>
        /// Adds a comment to task
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="commentatorEmail"></param>
        /// <param name="commentatorPassword"></param>
        /// <param name="taskOwnerEmail">Email of the task owner</param>
        /// <param name="taskToComment"></param>
        /// <param name="creationTime"></param>
        /// <returns>Is successfully</returns>
        public bool AddTaskComment(string comment, string commentatorEmail, string commentatorPassword,
            string taskOwnerEmail, TaskToDo taskToComment, DateTime creationTime)
        {
            if (taskToComment == null)
                return false;

            if (_usersList.Exists((x) => x.Email == taskOwnerEmail) &&
                _usersList.Exists((x) => x.Email == commentatorEmail))
            {
                User user = _usersList.Find((x) => x.Email == taskOwnerEmail);
                if (user.Tasks.Exists((x) => x == taskToComment))
                {
                    TaskToDo task = user.Tasks.Find((x) => x == taskToComment);
                    task.Comments.Add(new TaskComment(commentatorEmail, comment, creationTime));
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the comment from task
        /// </summary>
        /// <param name="commentToDelete"></param>
        /// <param name="commentOwnerEmail"></param>
        /// <param name="commentOwnerPassword"></param>
        /// <param name="taskOwnerEmail"></param>
        /// <returns>Is successfully</returns>
        public bool DeleteTaskComment(TaskComment commentToDelete, string commentOwnerEmail, string commentOwnerPassword,
            string taskOwnerEmail)
        {
            if (commentToDelete == null)
                return false;

            //verifying that the task owner exists
            if (!_usersList.Exists((x) => x.Email == taskOwnerEmail))
            {
                return false;
            }

            User user = _usersList.Find((x) => x.Email == taskOwnerEmail);
            foreach (TaskToDo task in user.Tasks) // search comment in every task
            {
                foreach (TaskComment comment in task.Comments)
                {
                    if (comment == commentToDelete)
                    {
                        task.Comments.Remove(comment);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Returns tasks list of the user
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public List<TaskToDo> GetUsersTasks(string email)
        {
            if (_usersList.Exists((x) => x.Email == email))
            {
                User user = _usersList.Find((x) => x.Email == email);
                //передаем ссылку на объект
                return new List<TaskToDo>(user.Tasks); //защита от изменения посторонними пользователями

            }
            return null;
        }
    }
}
