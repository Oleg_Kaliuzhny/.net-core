﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using IoC;
using IoC.Managers;
using System.Text.RegularExpressions;

namespace todoAppConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Container container = new Container();
            DataManager dataManager = container.GetDataManager();
            User currentUser = null;

            while (true)
            {
                Console.WriteLine("1 - Login");
                Console.WriteLine("2 - Show user's tasks");
                Console.WriteLine("3 - Show another user's tasks");
                Console.WriteLine("4 - Add user");
                Console.WriteLine("5 - Delete user");
                Console.WriteLine("6 - Add task");
                Console.WriteLine("7 - Add comment");
                Console.WriteLine("8 - Delete comment");
                Console.WriteLine("9 - Save to file");
                Console.WriteLine("0 - Load from file");
                Console.WriteLine("Another - exit");

                switch (Console.ReadLine())
                {
                    case "1":
                        currentUser = Login(dataManager);
                        break;
                    case "2":
                        ShowUsersTasks(dataManager, currentUser);
                        break;
                    case "3":
                        ShowUsersTasks(dataManager);
                        break;
                    case "4":
                        AddUser(dataManager);
                        break;
                    case "5":
                        DeleteUser(dataManager);
                        break;
                    case "6":
                        AddTask(dataManager, currentUser);
                        break;
                    case "7":
                        break;
                    case "8":
                        break;
                    case "9":
                        Save(dataManager);
                        break;
                    case "0":
                        Load(dataManager);
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        static void ShowUsersTasks(DataManager dataManager, User user = null)
        {
            string email = "";
            if (user is null)
            {
                Console.Write("Email of requested user: ");
                email = Console.ReadLine();
            }
            else
            {
                email = user.Email;
            }
            var tasks = dataManager.GetUsersTasks(email);
            if (tasks == null)
            {
                Console.WriteLine("Wrong email!");
                return;
            }

            var taskList = dataManager.GetUsersTasks(email);
            if (taskList.Count == 0)
                Console.WriteLine("Task list is empty.");
            foreach (var task in taskList)
            {
                Console.WriteLine("Task name: {0}", task.Title);
                Console.WriteLine("Task description:");
                Console.WriteLine(task.Description);
            }
        }

        static void AddUser(DataManager dataManager)
        {
            User newUser = new User();

            Console.Write("First Name: ");
            string firstName = Console.ReadLine();
            newUser.FirstName = firstName;

            Console.Write("Last Name: ");
            string lastName = Console.ReadLine();
            newUser.LastName = lastName;

            while (true)
            {
                Console.Write("Email: ");
                string email = Console.ReadLine();
                newUser.Email = email;
                if (newUser.Email == email)
                    break;
                Console.WriteLine("Wrong email!");
            }

            Console.Write("Password: ");
            string password = Console.ReadLine();
            newUser.PasswordHash = password.GetHashCode();

            dataManager.AddUser(newUser);

            Console.WriteLine("User has been added successfully.");
        }

        static void DeleteUser(DataManager dataManager)
        {
            Console.Write("User email to delete: ");
            string email = Console.ReadLine();
            Console.Write("User password to delete: ");
            string password = Console.ReadLine();
            dataManager.DeleteUser(email, password);
        }

        static void Load(DataManager dataManager)
        {
            if (dataManager.Load())
                Console.WriteLine("Loaded successfully.");
            else
                Console.WriteLine("Loading error. Something is wrong...");
        }

        static void Save(DataManager dataManager)
        {
            if (dataManager.Save())
                Console.WriteLine("Saved successfully.");
            else
                Console.WriteLine("Saving error. Something is wrong...");
        }

        static User Login(DataManager dataManager)
        {
            Console.Write("User email: ");
            string email = Console.ReadLine();

            Console.Write("User password: ");
            string password = Console.ReadLine();

            User user = dataManager.GetUser(email, password);
            if (user is null)
                Console.WriteLine("Incorrect login or password.");

            return user;
        }

        static void AddTask(DataManager dataManager, User user)
        {
            if (user is null)
            {
                Console.WriteLine("Please, log in.");
                return;
            }

            Console.Write("Title: ");
            string title = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();

            int priority = 0;
            while (true)
            {
                Console.Write("Priority (number): ");
                bool result = int.TryParse(Console.ReadLine(), out priority);
                if (result)
                    break;
            }

            Console.WriteLine("Beginning date");
            int year = 0;
            while (true)
            {
                Console.Write("Year (number): ");
                bool result = int.TryParse(Console.ReadLine(), out year);
                if (result)
                    break;
            };
            int month = 0;
            while (true)
            {
                Console.Write("Month (number): ");
                bool result = int.TryParse(Console.ReadLine(), out month);
                if (result)
                    break;
            }
            int day = 0;
            while (true)
            {
                Console.Write("Day (number): ");
                bool result = int.TryParse(Console.ReadLine(), out day);
                if (result)
                    break;
            };
            DateTime begin = new DateTime(year, month-1, day-1);

            Console.WriteLine("Finishing date");
            year = 0;
            while (true)
            {
                Console.Write("Year (number): ");
                bool result = int.TryParse(Console.ReadLine(), out year);
                if (result)
                    break;
            };
            month = 0;
            while (true)
            {
                Console.Write("Month (number): ");
                bool result = int.TryParse(Console.ReadLine(), out month);
                if (result)
                    break;
            };
            day = 0;
            while (true)
            {
                Console.Write("Day (number): ");
                bool result = int.TryParse(Console.ReadLine(), out day);
                if (result)
                    break;
            };
            DateTime finish = new DateTime(year, month-1, day-1);

            int hours = 0;
            while (true)
            {
                Console.Write("Nofity me in N hours after beginning: ");
                bool result = int.TryParse(Console.ReadLine(), out hours);
                if (result)
                    break;
            }

            Console.Write("Category: ");
            string category = Console.ReadLine();

            TaskToDo taskToDo = new TaskToDo(title, description, priority, Enums.TaskStatuses.Planned, 
                DateTime.Now, begin, finish, new List<DateTime>(), 
                new List<TaskComment>(), new List<Enums.TaskAttributes>(), category);

            taskToDo.NotificationSchedule.Add(new DateTime(year, month, day, hours, 0, 0));
            taskToDo.Attributes.Add(Enums.TaskAttributes.None);
            dataManager.AddUsersTask(user, taskToDo);
        }
    }
}
